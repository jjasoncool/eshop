# Local environment specification

1. PHP >= 7.3
2. MySQL >= 5.7.30

# Dev env setup

(Optional) Open a terminal for running docker

```sh
docker-compose up
```

Open another terminal for setup config

```sh
cp ./src/inc/cfg-temp.php ./src/inc/cfg.php
```

Then edit the config according to the dev env, if you use `docker-compose`, the setting is like:

```php
define('DB_HOST', 'db');
define('DB_PORT', 3306);
define('DB_USER', 'root');
define('DB_PASS', 'rootpassword');
define('DB_NAME', '');
define('root_depth', '1');
```

And here is other dev settings:

```php
define('LINE_API_ENDPOINT', 'https://sandbox-api-pay.line.me/v2/payments');
define('LINE_CHANNEL_ID', '1650369227');
define('LINE_CHANNEL_SECRET', '247deb96034cf7c4499e3e803c4925db');
```

Then you can see your project is hosted on http://localhost:5000, and dev mysql db is at localhost:5001.
