<?php
$index = true;
session_start();
session_destroy();
include "./inc/header.php";
?>
    <!-- Banner -->
        <section class="banner full">
            <?php
            for ($i = 1; $i < 2; $i++) {
                echo "<article><img src=\"images/main0{$i}.jpg\" alt=\"\">";
                if (false) {
                    echo "<div class=\"inner\" style=\"background-color: rgba(0,0,0,0.3);\">
                    <header>
                    <p>粉領暴走族！應援走起來♥</p>
                    <h2>2020 粉紅健走</h2>
                    </header>";
                }
                echo "</div></article>";
            }
            ?>
        </section>

    <!-- 故事 -->
        <section id="story" class="wrapper style2" style="padding-bottom: 0px;">
            <div class="inner">
                <header class="align-center">
                    <h2>【台癌粉紅健走】報名即送「某人日常」運動毛巾+提袋 快揪團來報名!</h2>
                </header>
                <span class="image right"><img src="images/somebody.png" alt="某人也來造"></span>
                <b>乳癌是女性十大癌症第一位，其發生率以翻倍的速度攀升!</b><br><br>
                台灣癌症基金會每年皆會舉行「粉紅健走」，今年雖恰逢疫情關係，健走延期至9月27日（日），<br>
                但台癌再次與某人日常攜手，靜靜的看著你們走，並為10月的國際乳癌防治月作先鋒開走。<br><br>
                只要捐款<b>420元</b>，便獲得<b>報名資格1名與福袋禮品！</b>福袋包含由某人日常設計的超獨家<b>「運動毛巾」</b>與<b>「提袋」</b>(禮物樣式以實體為準)，其中運動毛巾某人更是大方送設計了三款，分別代表預防乳癌三大面向:
                <ul style="margin:0;">
                    <li>「EXERCISE&RELAX」（黃色毛巾：規律運動、心情放鬆減低工作壓力）</li>
                    <li>「SCREENING&FIT」（粉色毛巾：定期篩檢&體重控制）</li>
                    <li>「VIGGIES&FRUITS」（綠色毛巾：蔬果彩虹579）</li>
                </ul>
                報名時記得挑選好你要帶走哪一個某人陪你一起健走唷！<br><br>
                <div>
                    <div class="image fit">
                       <img src="images/maingift.png" alt="" style="max-width:600px">
                    </div>
                </div>

                趕快揪團來報名粉紅健走，逗陣顧健康、拿好康～<br>
                不只這樣，還有其他的報名禮、完走禮、摸彩禮等等，好禮拿到讓你不好意思！<br><br>
                您所捐款報名的每筆費用，將妥善的運用至乳癌防治推廣與乳癌病友關懷活動上，誠摯邀請您共創粉紅風潮，一起用行動關心支持身邊的每一個「她」。<br><br>

                ※毛巾各款數量有限，請儘早報名「確保」某人！<br>
                ※為保障參與人員的權益，路跑當天發送以報名資料為準，無法換款。<br>

                <b>◎健走時間：2020年9月27日 (日) AM 8:30集合</b><br>
                <b>◎集合地點：圓山花博花海廣場(捷運圓山站出口往花海廣場方向) 全程約7公里</b><br>
                <b>◎報名人數：4,500人(額滿為止，報名要快喔!)</b><br>
                <b>◎報名捐款費用： 420元/人</b> ※10人(含)以上報名享有優惠價400元/人<br>
                <b>◎網路報名：<a href="register.php">【進入線上報名系統】</a></b><br>
                <b>◎健走洽詢專線：(02)8787-9907分機205、220</b>

            </div>
        </section>
    <!-- One -->
        <section id="one" class="wrapper style2">
            <div class="inner">
                <div class="row">
                    <div class="4u 12u$(medium)">
                        <div class="box">
                            <div class="content">
                                <header class="align-center">
                                    <h2>網路報名</h2>
                                </header>
                                <p>請至<a href="register.php">【我要報名】</a>或按下方"開始報名"<br>填寫資料後，再選擇繳款方式，完成報名後<br>將收到「粉紅健走報名確認」E-mail<br>即完成報名手續。</p>
                                <footer class="align-center">
                                    <a href="register.php" class="button alt">開始報名</a>
                                </footer>
                            </div>
                        </div>
                    </div>

                    <div class="4u 12u$(medium)">
                        <div class="box">
                            <div class="content">
                                <header class="align-center">
                                    <h2>傳真/郵寄報名</h2>
                                </header>
                                <p>請工整填寫報名資訊並附上付款明細(郵政劃撥收據 or 信用卡授權書)，一起傳真到02-8787-9222後，來電02-8787-9907#205確認，即完成報名手續</p>
                                <footer class="align-center">
                                    <a href="/annex/PinkWalk_Form.pdf" class="button alt" target="_blank">下載報名表</a>
                                </footer>
                            </div>
                        </div>
                    </div>

                    <div class="4u 12u$(medium)">
                        <div class="box">
                            <div class="content">
                                <header class="align-center">
                                    <h2>親赴報名</h2>
                                </header>
                                <p>您就在台灣癌症基金會附近嗎？<br><b>週一~週五 上午9:30~下午5:30</b><br>都可以親洽台灣癌症基金會櫃台報名繳費哦<br>（台北市松山區南京東路五段16號5樓之2）</p>
                                <footer class="align-center">
                                    <a href="https://goo.gl/maps/FaBawheq7FL8nf4n6" target="_blank" class="button alt">看看在哪</a>
                                </footer>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>


    <!-- Two -->
        <section id="two" class="wrapper style3">
            <div class="inner">
                <header class="align-center">
                    <p style="color:#003D79; font-weight:bold">請密切關注我們的活動最新訊息</p>
                    <h2 style="color:#003D79; font-weight:bold">最新消息</h2>
                </header>
                <div class="content">
                    <div class="box" style="background-color: rgba(0,0,0,0.6);">
                        <header>
                            <h3>
                                <img src="./images/pin.png" alt="" width="32px" heigth="32px" style="vertical-align: baseline">
                                <span style="font-weight:bold">2020/07/14</span>
                            </h3>
                        </header>
                        <div style="margin:0 3em; font-weight:bold">2020粉紅健走嘉年華開始報名囉!! <a href="/annex/PinkWalk_Form.pdf" target="_blank">點我下載</a></div>
                    </div>
                </div>
            </div>
        </section>

    <!-- Three -->
        <!-- <section id="three" class="wrapper style2">
            <div class="inner">
                <header class="align-center">
                    <p class="special">讓我們的活動更順利的夥伴~</p>
                    <h2>贊助廠商</h2>
                </header>
                <div class="gallery">
                    <div>
                        <div class="image fit">
                            <a href="#"><img src="images/pic01.jpg" alt="" /></a>
                        </div>
                    </div>
                    <div>
                        <div class="image fit">
                            <a href="#"><img src="images/pic02.jpg" alt="" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->

<?php include "./inc/footer.php"; ?>
