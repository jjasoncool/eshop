<?php
require_once "LINEPay.php";

session_start();
if (
    empty($_SESSION)
    || isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 900)
) {
    // 驗證該有的東西是否都沒有少，若少踢出去
    session_destroy(); // destroy session data in storage
    header("location: ./register.php");
    exit();
} else {
    // 上一頁回到此頁面，重設逾時時間
    $_SESSION['LAST_ACTIVITY'] = time();
    $pay_mount = $_SESSION["pay_mount"];
    $orderId = $_SESSION["orderId"];
    $regid = $_SESSION["regid"];
}

// 主機資訊
$serverName = $_SERVER["SERVER_NAME"];
$url = "https://{$serverName}";

$data = array(
    "productName" => "PinkWalk 粉紅健走",
    "productImageUrl" => "{$url}/images/icon.png",
    "amount" => $pay_mount,
    "currency" => "TWD",
    "confirmUrl" => "{$url}/payment/LinePay/Confirm.php",
    "cancelUrl" =>  "{$url}/Cancel.php",
    "orderId" => $orderId,

    // 使用者確認付款後，confirmUrl的作用如下。
    // CLIENT - 使用者的畫面跳轉到商家confirmUrl，完成付款流程
    // SERVER - LINE Pay Server向Merchant Server請求confirmUrl
    "confirmUrlType" => "CLIENT",
    "checkConfirmUrlBrowser" => true,

    // If set "PREAPPROVED", need to handle "regKey"
    "payType" => "NORMAL",

    // If set false, need to handle how to continuous the process
    "capture" => true
);

$response = LINEPay::reserve($data);

// show the label for next step if it success
$ack = json_decode($response);
// 0000 才是成功付款
if ($ack->returnCode == "0000") {
    $paymentURL = $ack->info->paymentUrl->web;
    header("Location: " . $paymentURL);
    exit;
} else {
    header("Location: ../../pay_fail.php");
    exit;
}