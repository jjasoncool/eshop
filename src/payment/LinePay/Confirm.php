<?php

header('Cache-Control: no-cache, no-store, must-revalidate');
header('Expires: Thu, 01 Jan 1970 00:00:00 GMT');
header('Pragma: no-cache');
require_once "LINEPay.php";
require_once "../../inc/cfg.php";
// 當LINE Pay請求付款時，回應的confirmUrl中Query String包含著transactionId和orderId。
$transactionId = reqParam('transactionId', 'get');

session_start();
if (
    empty($_SESSION)
    || isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 900)
    || empty($transactionId)
) {
    // 驗證該有的東西是否都沒有少，若少踢出去
    session_destroy(); // destroy session data in storage
    header("location: /");
    exit();
} else {
    // 上一頁回到此頁面，重設逾時時間
    $_SESSION['LAST_ACTIVITY'] = time();
    // 當LINE Pay請求付款時，回應的confirmUrl中Query String包含著transactionId和orderId。
    $pay_mount = $_SESSION["pay_mount"];
    $orderId = $_SESSION["orderId"];
    $regid = $_SESSION["regid"];
}

$data = array(
    "amount" => $_SESSION["pay_mount"],
    "currency" => "TWD",
);

$response = json_decode(LINEPay::confirm($transactionId, $data), true);

if (strpos(strtolower($response['returnMessage']), 'success') !== false) {
    $db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);
    $dt = new dateTime();
    $updColArray = [
        'pay_return_info' => print_r($response, true),
        'check_pay' => $dt->format('Y-m-d H:i:s'),
    ];

    $updColStr = '';
    foreach ($updColArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    $dbQuery = "UPDATE FCF_pinkwalk.registlist
                SET {$updColStr}
                WHERE idno=? AND pay_orderid=?";
    $db->query($dbQuery, array_merge(array_values($updColArray), [$regid, $orderId]));

    header("Location: ../../pay_success.php?n=2");
    exit;
} else {
    header("Location: ../../pay_fail.php");
    exit;
}
