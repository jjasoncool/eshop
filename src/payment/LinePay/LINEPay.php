<?php

require_once '../../inc/cfg.php';

class LINEPay
{
    public static function reserve($data)
    {
        return self::post(LINE_API_ENDPOINT . '/request', $data);
    }

    public static function confirm($transactionId, $data)
    {
        return self::post(LINE_API_ENDPOINT . '/' . $transactionId . '/confirm', $data);
    }

    private static function post($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        // Line Pay Required TLS version: 1.2
        curl_setopt($ch, CURLOPT_SSLVERSION, 'CURL_SSLVERSION_TLSv1_2');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type:application/json; charset=UTF-8',
            'X-LINE-ChannelId:' . LINE_CHANNEL_ID,
            'X-LINE-ChannelSecret:' . LINE_CHANNEL_SECRET,
        ]);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }

        $postResult = curl_exec($ch);

        if (curl_errno($ch)) {
            echo curl_error($ch);
        }

        return $postResult;
    }
}
