<?php
require_once "../../inc/cfg.php";

session_start();
if (empty($_SESSION) || isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 900)) {
    // 15分鐘逾時
    session_destroy(); // destroy session data in storage
    header("location: ./register.php");
    exit();
} else {
    // 上一頁回到此頁面，重設逾時時間
    $_SESSION['LAST_ACTIVITY'] = time();
    $pay_mount = $_SESSION["pay_mount"];
    // ECPAY 交易編號只能20碼
    $orderId = substr($_SESSION["orderId"], 0, 20);
    $regid = $_SESSION["regid"];

    $db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);
    $dbQuery = "UPDATE FCF_pinkwalk.registlist SET pay_orderid=? WHERE idno=?";
    $db->query($dbQuery, [$orderId, $regid]);
    $_SESSION["orderId"] = $orderId;
}

?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>線上刷卡</title>
</head>
<body>

<?php
// 特殊字元置換
function _replaceChar($value)
{
    $search_list = array('%2d', '%5f', '%2e', '%21', '%2a', '%28', '%29');
    $replace_list = array('-', '_', '.', '!', '*', '(', ')');
    $value = str_replace($search_list, $replace_list, $value);

    return $value;
}
// 產生檢查碼
function _getMacValue($hash_key, $hash_iv, $form_array)
{
    $encode_str = "HashKey=" . $hash_key;
    foreach ($form_array as $key => $value) {
        $encode_str .= "&" . $key . "=" . $value;
    }
    $encode_str .= "&HashIV=" . $hash_iv;
    $encode_str = strtolower(urlencode($encode_str));
    $encode_str = _replaceChar($encode_str);
    return md5($encode_str);
}
//------------------------------------------交易輸入參數------------------------------------------------------
$serverName = $_SERVER["SERVER_NAME"];
$serverPort = $_SERVER["SERVER_PORT"];
$url = "https://{$serverName}";

// 訂單編號
$trade_no = $orderId;
// 交易金額
$total_amt = $pay_mount;
// 交易描述
$trade_desc = "線上刷卡支付";
// 如果商品名稱有多筆，需在金流選擇頁一行一行顯示商品名稱的話，商品名稱請以井號分隔(#)
$item_name = "PinkWalk 粉紅健走";
// 交易返回頁面 (to server update)
$return_url = "{$url}/payment/ecPay/Confirm.php";
// 交易通知網址 (show to client page)
$client_back_url = "{$url}/pay_success.php?n=1";
// 選擇預設付款方式
$choose_payment = "Credit";
// 是否需要額外的付款資訊
$needExtraPaidInfo = "N";
// Alipay 必要參數
$alipay_item_name = $item_name;
$alipay_item_counts = 1;
$alipay_item_price = $total_amt;
$alipay_email = '579aday@canceraway.org.tw';
$alipay_phone_no = '0287879907';
$alipay_user_name = 'canceraway87879907';
/***************以下為測試環境資訊(若轉換為正式環境,請修改以下參數值)**********************/
// 測試環境
// $gateway_url = "https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5";
// $merchant_id = "2000132";
// $hash_key = "5294y06JbISpM5x9";
// $hash_iv = "v77hoKGq4kWxNNIS";

// 生產環境
$gateway_url = ECPAY_API_URL;
$merchant_id = ECPAY_ID;
$hash_key = ECPAY_KEY;
$hash_iv = ECPAY_IV;
/**********************************************************************************/
$form_array = array(
    "ChoosePayment" => $choose_payment,
    "ClientBackURL" => $client_back_url,
    "ClientRedirectURL" => $client_back_url,
    "ItemName" => $item_name,
    "MerchantID" => $merchant_id,
    "MerchantTradeDate" => date("Y/m/d H:i:s"),
    "MerchantTradeNo" => $trade_no,
    "NeedExtraPaidInfo" => $needExtraPaidInfo,
    "OrderResultURL" => $client_back_url,
    "PaymentType" => "aio",
    "ReturnURL" => $return_url,
    "TotalAmount" => $total_amt,
    "TradeDesc" => $trade_desc,
);

# 取得 Mac Value
$form_array['CheckMacValue'] = _getMacValue($hash_key, $hash_iv, $form_array);

$html_code = "<form name=\"ecpay\" method=\"post\" action=\"{$gateway_url}\">";
foreach ($form_array as $key => $val) {
    $html_code .= "<input type='hidden' name='{$key}' value='{$val}'><br>";
}
echo $html_code;
?>

<script language=javascript>
	document.forms.ecpay.submit();
</script>
</body>
</html>