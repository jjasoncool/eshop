<?php
require_once "../../inc/cfg.php";

header('Cache-Control: no-cache, no-store, must-revalidate');
header('Expires: Thu, 01 Jan 1970 00:00:00 GMT');
header('Pragma: no-cache');

$orderId = reqParam('MerchantTradeNo', 'post');
$successCode = reqParam('RtnCode', 'post');
$paydatetime = strtr(reqParam('PaymentDate', 'post'), ['/' => '-']);
$returnInfo = print_r($_POST, true);

// 經測試後，返回值如下
// Array
// (
//     [CustomField1] =>
//     [CustomField2] =>
//     [CustomField3] =>
//     [CustomField4] =>
//     [MerchantID] => 2000132
//     [MerchantTradeNo] => 20200712054801147977
//     [PaymentDate] => 2020/07/12 05:48:35
//     [PaymentType] => Credit_CreditCard
//     [PaymentTypeChargeFee] => 9
//     [RtnCode] => 1
//     [RtnMsg] => 交易成功
//     [SimulatePaid] => 0
//     [StoreID] =>
//     [TradeAmt] => 420
//     [TradeDate] => 2020/07/12 05:48:03
//     [TradeNo] => 2007120548033777
//     [CheckMacValue] => 5AC3A6FCD3C46E64ACE85F846381B33D
// )

if ($successCode == 1 && !empty($orderId)) {
    $db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);
    $dbQuery = "UPDATE FCF_pinkwalk.registlist SET pay_return_info=?, check_pay=? WHERE pay_orderid=? AND pay_method=1";
    $db->query($dbQuery, [$returnInfo, $paydatetime, $orderId]);
}