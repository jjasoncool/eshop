<?php
session_start();
$index = false;
include "./inc/header.php";

// 付款失敗的話，禮物不能算
$regid = $_SESSION["regid"];
$db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);
$dbQuery = "UPDATE FCF_pinkwalk.registlist SET ct_gift=0 WHERE idno=?";
$db->query($dbQuery, [$regid]);
?>
<!-- One -->
    <section id="One" class="wrapper style3">
        <div class="inner">
            <header class="align-center">
                <img src="images/register.png" style="max-height:300px; max-width:100%">
            </header>
        </div>
    </section>
    <section id="two" class="wrapper style2">
        <div class="inner">
            <div class="box">
                <div class="content">
                    <header class="align-center">
                        <img src="./images/pin.png" alt="" width="32px" heigth="32px" style="vertical-align: baseline">
                        <h2>線上付款失敗</h2>
                    </header>
                    <div class="align-center">
                        <p>您的線上作業失敗，建議您聯絡您的信用卡發卡銀行、確認LINEPAY綁定之信用卡額度或信用卡驗證資訊。<br>
                            您也可以更換其他種繳費方式，或其他問題歡迎致電02-87879907分機206洽詢。<br></p>
                        <ul class="actions fit">
                            <li><a class="button special fit" href="./register.php">重新填寫報名資料</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include "./inc/footer.php"; ?>