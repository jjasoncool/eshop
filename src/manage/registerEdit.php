<?php
require "adminHead.php";
require "navbar.php";

$db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);
$id = $db->escape(reqParam('id', 'get'));

$dbQuery = "SELECT * FROM FCF_pinkwalk.registlist WHERE idno=?";
$result = $db->row($dbQuery, [$id]);
$modalData = $result;

$dbQuery = "SELECT * FROM FCF_pinkwalk.attendlist WHERE reg_id=?";
$attresult = $db->query($dbQuery, [$id]);
$attCount = count($attresult) + 1;
$showattend = '';
foreach ($attresult as $row) {
    $row['att_sex'] = ($row['att_sex'] == 'male') ? '男' : '女';
    $row['att_hope'] = ($row['att_hope'] == 1) ? '有' : '無';
    switch ($row['att_gift']) {
        case '1':
            $row['att_gift'] = '「EXERCISE&RELAX」<br>（黃色毛巾：規律運動、心情放鬆減低工作壓力）';
            break;
        case '2':
            $row['att_gift'] = '「SCREENING&FIT」<br>（粉色毛巾：定期篩檢&體重控制）';
            break;
        case '3':
            $row['att_gift'] = '「VIGGIES&FRUITS」<br>（綠色毛巾：蔬果彩虹579）';
            break;

        default:
            $row['att_gift'] = '未選禮品';
            break;
    }

    $showattend .= <<<EOD
    <tr>
        <td>{$row['att_name']}</td>
        <td>{$row['att_sex']}</td>
        <td>{$row['att_birth']}</td>
        <td>{$row['att_hope']}</td>
        <td>{$row['att_phone']}</td>
        <td>{$row['att_gift']}</td>
    </tr>
    EOD;
    // <td><button class="btn btn-sm btn-danger delattend" value="{$row['idno']}">刪除</button></td>
}
?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">
                    <b><?="#{$result['idno']}"?></b> 詳細資料
                    <a href="javascript:history.back();" style="font-size: 14px;">回上一頁</a>
                </h2>
                <!-- Trigger the modal with a button -->
                <button type="button" class="btn btn-info" style="margin-bottom: 1rem;" data-toggle="modal" data-target="#modModal">修改資料</button>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th colspan="4">報名資訊</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="25%">單位團體<br>
                                    <span class="text-info"><b><?=$result['ct_unit']?></b></span>
                                </td>
                                <td width="25%">聯絡人姓名<br>
                                    <span class="text-info"><b><?=$result['ct_name']?></b></span>
                                </td>
                                <td width="25%">聯絡人生日<br>
                                    <span class="text-info"><b><?=$result['ct_birth']?></b></span>
                                </td>
                                <td width="25%">聯絡人性別<br>
                                    <span class="text-info"><b><?=$result['ct_sex'] == 'male' ? '男' : '女'?></b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>希望護照<br>
                                    <span class="text-info"><b><?=$result['ct_hope'] == 1 ? '有' : '無'?></b></span>
                                </td>
                                <td>連絡電話<br>
                                    <span class="text-info"><b><?=$result['ct_phone']?></b></span>
                                </td>
                                <td>手機<br>
                                    <span class="text-info"><b><?=$result['ct_mobile']?></b></span>
                                </td>
                                <td>Email<br>
                                    <span class="text-info"><b><?=$result['ct_email']?></b></span>
                                </td>
                            </tr>
                            <tr>
                                <td>居住地址<br>
                                    <span class="text-info"><b><?=$result['ct_address']?></b></span>
                                </td>
                                <td>捐款收據<br>
                                    <?php
                                    switch ($result['ct_receipt']) {
                                        case '1':
                                            $showreceipt = '個人報名';
                                            break;
                                        case '2':
                                            $showreceipt = '團體報名分別開立';
                                            break;
                                        case '3':
                                            $showreceipt = '團體報名統一開立';
                                            break;
                                        case '4':
                                            $showreceipt = '不需要捐款收據';
                                            break;

                                        default:
                                            $showreceipt = '';
                                            break;
                                    }
                                    ?>
                                    <span class="text-info"><b><?=$showreceipt?></b></span>
                                </td>
                                <td>收據開立名稱 / 統一編號(身分證字號)<br>
                                    <span class="text-info">
                                        <b>
                                        <?=$result['ct_receiptname']?>
                                        <?=(!empty($result['ct_receiptname']) && empty($result['ct_uniform_no'])) ? '' : '/'?>
                                        <?=$result['ct_uniform_no']?>
                                        </b>
                                    </span>
                                </td>
                                <td>報名人數<br>
                                    <span class="text-info"><b><?=$attCount?></b>人</span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">報名禮品<br>
                                    <?php
                                    switch ($result['ct_gift']) {
                                        case '1':
                                            $showgift = '「EXERCISE&RELAX」（黃色毛巾：規律運動、心情放鬆減低工作壓力）';
                                            break;
                                        case '2':
                                            $showgift = '「SCREENING&FIT」（粉色毛巾：定期篩檢&體重控制）';
                                            break;
                                        case '3':
                                            $showgift = '「VIGGIES&FRUITS」（綠色毛巾：蔬果彩虹579）';
                                            break;

                                        default:
                                            $showgift = '未選禮品';
                                            break;
                                    }
                                    ?>
                                    <span class="text-info"><b><?=$showgift?></b></span>
                                </td>
                                <td>報名時間<br>
                                    <span class="text-info"><b><?=$result['key_date']?></b></span>
                                </td>
                                <td>公開捐款資訊<br>
                                    <span class="text-info"><b><?=($result['ct_public'] == 1) ? '是' : '否'?></b></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
                <?php
                if (!empty($showattend)) {
                ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>參加者姓名</th>
                                <th>性別</th>
                                <th>生日</th>
                                <th>希望護照</th>
                                <th>連絡電話</th>
                                <th>選擇好禮</th>
                                <!-- <th></th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?=$showattend?>
                        </tbody>
                    </table>
                </div>
                <?php
                }
                ?>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th colspan="4">付款資訊 <?=(empty($result['check_pay'])) ? "<button type=\"button\" id=\"toggle_check_pay\">變更繳費狀況</button>" : ''?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="25%">報名費用<br>
                                    <span class="text-info"><b><?=intval($result['pay_mount']) - intval($result['ct_donate'])?></b></span>
                                </td>
                                <td width="25%">額外捐款<br>
                                    <span class="text-info"><b><?=$result['ct_donate']?></b></span>
                                </td>
                                <td width="25%">支付方式<br>
                                    <?php
                                    switch ($result['pay_method']) {
                                        case '1':
                                            $showpay = '線上刷卡';
                                            break;
                                        case '2':
                                            $showpay = 'LINEPAY';
                                            break;
                                        case '3':
                                            $showpay = '信用卡授權傳真';
                                            break;
                                        case '4':
                                            $showpay = '郵政劃撥';
                                            break;
                                        case '5':
                                            $showpay = '現金支付';
                                            break;

                                        default:
                                            $showpay = '無效資料';
                                            break;
                                    }
                                    ?>
                                    <span class="text-info"><b><?=$showpay?></b></span>
                                </td>
                                <td width="25%">繳費狀況<br>
                                    <span class="text-info"><b id='check_pay'><?=is_null($result['check_pay']) ? '尚未繳款' : "{$result['check_pay']} 已繳款"?></b></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="modModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">修改資料</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-6">
                        <label><b>單位團體</b></label><br>
                        <input type="text" class="form-control" name="ct_unit" value="<?=$modalData['ct_unit']?>">
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>聯絡人姓名</b></label><br>
                        <input type="text" class="form-control" name="ct_name" value="<?=$modalData['ct_name']?>">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label><b>聯絡人性別</b></label><br>
                        <input type="radio" class="form-control-inline" name="ct_sex" value="male" <?=$modalData['ct_sex'] == 'male' ? 'checked' : ''?>>男
                        <input type="radio" class="form-control-inline" name="ct_sex" value="female" <?=$modalData['ct_sex'] == 'female' ? 'checked' : ''?>>女
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>希望護照</b></label><br>
                        <input type="checkbox" class="form-control-inline" name="ct_hope" value="1" <?=$modalData['ct_hope'] == '1' ? 'checked' : ''?>>有
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label><b>聯絡人生日</b></label><br>
                        <input type="text" class="form-control" name="ct_birth" id="ct_birth" value="<?=$modalData['ct_birth']?>">
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>聯絡電話</b></label><br>
                        <input type="text" class="form-control" name="ct_phone" id="ct_phone" value="<?=$modalData['ct_phone']?>">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label><b>手機</b></label><br>
                        <input type="text" class="form-control" name="ct_mobile" id="ct_mobile" value="<?=$modalData['ct_mobile']?>">
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>Email</b></label><br>
                        <input type="text" class="form-control" name="ct_email" id="ct_email" value="<?=$modalData['ct_email']?>">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label><b>居住地址</b></label><br>
                        <input type="text" class="form-control" name="ct_address" id="ct_address" value="<?=$modalData['ct_address']?>">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label><b>收據開立名稱</b></label><br>
                        <input type="text" class="form-control" name="ct_receiptname" id="ct_receiptname" value="<?=$modalData['ct_receiptname']?>">
                    </div>
                    <div class="form-group col-md-6">
                        <label><b>統一編號(身分證字號)</b></label><br>
                        <input type="text" class="form-control" name="ct_uniform_no" id="ct_uniform_no" value="<?=$modalData['ct_uniform_no']?>">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label><b>支付方式</b></label><br>
                        <select class="form-control" name="pay_method" id="pay_method">
                            <option value="1" <?=$modalData['pay_method'] == 1 ? 'selected' : ''?>>線上刷卡</option>
                            <option value="2" <?=$modalData['pay_method'] == 2 ? 'selected' : ''?>>LINEPAY</option>
                            <option value="3" <?=$modalData['pay_method'] == 3 ? 'selected' : ''?>>信用卡授權傳真</option>
                            <option value="4" <?=$modalData['pay_method'] == 4 ? 'selected' : ''?>>郵政劃撥</option>
                            <option value="5" <?=$modalData['pay_method'] == 5 ? 'selected' : ''?>>現金支付</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="modData">儲存</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

<script>
    let modalData = <?=json_encode($modalData, JSON_UNESCAPED_UNICODE)?>
    // register edit
    function init() {
        const $toggleCheckPay = $('#toggle_check_pay');
        const $modData = $('#modData');
        $toggleCheckPay.click(() => toggleCheckPay(<?=$id?>, <?=isset($result['check_pay']) ? 'true' : 'false'?>));
        $modData.click(() => modData(<?=$id?>));
        $("#ct_birth").flatpickr();

        window.toggleCheckPay = (id, check_pay) => {
            $toggleCheckPay
                .off("click")
                .attr("disabled", true);

            updateRegister(
                id,
                {
                    check_pay: check_pay ? null: new Date().toISOString().slice(0, 19).replace('T', ' '),
                },
                {
                    onSuccess: function(resp){
                        const { check_pay: respCheckPay } = JSON.parse(resp);
                        $('#check_pay').text(respCheckPay ? '已繳款' : '尚未繳款');
                        $toggleCheckPay.remove();

                        setTimeout(() => {
                            // $toggleCheckPay
                            //     .click(() => toggleCheckPay(id, respCheckPay))
                            //     .removeAttr("disabled");
                        }, 1000);
                    },
                    onFail: function(xhr, status, error) {
                        // TODO: error handling
                        console.error(error)
                    },
                },
            );
        }

        window.modData = (id) => {
            const $modModal = $("#modModal");
            $modData
                .off("click")
                .attr("disabled", true);

            updateRegisterInfo(
                id,
                {
                    ct_unit : $("#modModal [name='ct_unit']").val(),
                    ct_name : $("#modModal [name='ct_name']").val(),
                    ct_sex : $("#modModal [name='ct_sex']:checked").val(),
                    ct_hope : ($("#modModal [name='ct_hope']").prop("checked") ? 1 : 0),
                    ct_birth : $("#modModal [name='ct_birth']").val(),
                    ct_phone : $("#modModal [name='ct_phone']").val(),
                    ct_mobile : $("#modModal [name='ct_mobile']").val(),
                    ct_email : $("#modModal [name='ct_email']").val(),
                    ct_address : $("#modModal [name='ct_address']").val(),
                    ct_receiptname : $("#modModal [name='ct_receiptname']").val(),
                    ct_uniform_no : $("#modModal [name='ct_uniform_no']").val(),
                    pay_method : $("#modModal [name='pay_method']").val(),
                },
                {
                    onSuccess: function(resp){
                        location.reload();
                    },
                    onFail: function(xhr, status, error) {
                        // TODO: error handling
                        console.error(error)
                    },
                },
            );
        }

        function updateRegister(id, payload, {onSuccess = () => {}, onFail = () => {}} = {}) {
            $.post(
                'api/register/update.php',
                {
                    id,
                    payload,
                },
            )
            .done(onSuccess)
            .fail(onFail);
        }

        function updateRegisterInfo(id, infoData, {onSuccess = () => {}, onFail = () => {}} = {}) {
            $.post(
                'api/register/update.php',
                {
                    id,
                    payload: infoData,
                },
            )
            .done(onSuccess)
            .fail(onFail);
        }

    }

    window.onload = init;
</script>
<?php require "adminFooter.php"; ?>