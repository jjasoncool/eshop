<?php
    session_start();
    if (!isset($_SESSION['userid']) && $_SERVER['REQUEST_URI'] !== '/manage/login.php') {
        session_destroy();
        session_unset();
        header("Location: /manage/login.php");
        exit();
    };

    // Auto-logout
    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 14400)) {
        // last request was more than 3 hours ago
        session_destroy(); // destroy session data in storage
        session_unset(); // unset $_SESSION variable for the runtime
        header("location: /manage/login.php?msg=1"); // "Automatically logout after idle for a long time.."
        exit();
    }
    // update last activity time stamp
    $_SESSION['LAST_ACTIVITY'] = time();
?>