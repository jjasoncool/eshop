<?php
    require_once "../../inc/cfg.php";
    session_start();
    $db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);

    // username and password sent from form
    $username = $db->escape(trim(reqParam('username', 'post')));
    $password = $db->escape(trim(reqParam('password', 'post')));

    $dbQuery = "SELECT * FROM users WHERE username = ?";
    $result = $db->row($dbQuery, array($username));

    $db->closeConnection();

    if ($password === $result['password']) {
        $ip = getIP();
        // security
        $_SESSION['footprint'] = md5($ip . $_SERVER['HTTP_USER_AGENT']);
        $_SESSION['LAST_ACTIVITY'] = time();

        // session 帳號基本資料
        $_SESSION['userid'] = $result['id'];
        $_SESSION['username'] = $result['username'];

        header("location: /manage");
    } else {
        header("location: /manage/login.php?msg=2");
        die();
    }
