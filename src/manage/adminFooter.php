        </div>
        <!-- /#wrapper -->

        <!-- jQuery -->
        <script src="../assets/js/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="../assets/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="../assets/js/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="../assets/js/raphael.min.js"></script>
        <script src="../assets/js/morris.min.js"></script>

        <!-- DataTables JavaScript -->
        <script src="../assets/js/dataTables/jquery.dataTables.min.js"></script>
        <script src="../assets/js/dataTables/dataTables.bootstrap.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="../assets/js/startmin.js"></script>

        <script src="../assets/js/flatpickr.js"></script>

    </body>
</html>