<?php

require_once '../guard.php';
require_once '../../../inc/cfg.php';
$db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);

$id = $db->escape(trim(reqParam('id', 'post')));
$payload = reqParam('payload', 'post');
$updateFields = '';

// sanitize
foreach ($payload as $key => $value) {
    $payload[$key] = $db->escape(trim($value));

    if ($key === 'check_pay' && $payload[$key] === '') {
        $payload[$key] = null;
    }

    $updateFields .= $key . ' = :' . $key;

    if ($key !== array_key_last($payload)) {
        $updateFields .= ', ';
    }
}

$updateQuery = "UPDATE
                    FCF_pinkwalk.registlist
                SET
                    {$updateFields}
                WHERE
                    idno = :idno;";

$db->query(
    $updateQuery,
    array_merge(
        array('idno' => $id),
        $payload
    )
);

$dbQuery = "SELECT
                *
            FROM FCF_pinkwalk.registlist
            WHERE
                idno = :id;";

$result = $db->row(
    $dbQuery,
    array('id' => $id)
);

echo json_encode($result);
