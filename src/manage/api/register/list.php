<?php

require_once '../guard.php';
require_once '../../../inc/cfg.php';
$db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);

$draw = intval($db->escape(reqParam('draw', 'get')));
$start = intval($db->escape(reqParam('start', 'get')));
$length = intval($db->escape(reqParam('length', 'get')));
$search = $db->escape(reqParam('search', 'get')['value']);
$payfee = boolval($db->escape(reqParam('fee', 'get')));
if ($payfee) {
    $where = " AND check_pay IS NOT NULL ";
} else {
    $where = " AND check_pay IS NULL AND ct_gift!=0";
}

$dbQuery = "SELECT idno,
                ct_name,
                DATE(key_date),
                ct_mobile,
                pay_mount,
                check_pay
            FROM FCF_pinkwalk.registlist
            WHERE (LOCATE(?, ct_name)>0 || LOCATE(?, key_date)>0) {$where}
            LIMIT ?, ?";

$result = $db->query($dbQuery, ["{$search}", "{$search}", $start, $length], PDO::FETCH_NUM);

// 算清單項目數量
$recordsNumsQuery = "SELECT COUNT('idno') AS recordsTotal FROM FCF_pinkwalk.registlist WHERE ct_gift!=0;";
$recordsTotal = $db->row($recordsNumsQuery)['recordsTotal'];
$recordsFilteredNumsQuery = "SELECT COUNT('idno') AS recordsTotal
                    FROM FCF_pinkwalk.registlist
                    WHERE (LOCATE(?, ct_name)>0 || LOCATE(?, key_date)>0) {$where};";
$recordsFiltered = $db->row($recordsFilteredNumsQuery, ["{$search}", "{$search}"])['recordsTotal'];

echo json_encode(
    [
        'draw' => $draw,
        'recordsTotal' => $recordsTotal,
        'recordsFiltered' => $recordsFiltered,
        'data' => $result,
    ]
);
