<?php
session_start();
if (!isset($_SESSION['userid'])) {
    session_destroy();
    session_unset();
    header("HTTP/1.1 401 Unauthorized");
    exit;
}

