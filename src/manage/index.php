<?php
require "adminHead.php";
require "navbar.php";

$db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);
$dbQuery = "SELECT sex, COUNT(1) AS sexnum
            FROM
            (
            SELECT ct_sex AS `sex`, ct_gift AS gift
            FROM FCF_pinkwalk.registlist UNION ALL
            SELECT att_sex AS `sex`, att_gift AS gift
            FROM FCF_pinkwalk.attendlist) g
            WHERE g.gift!=0
            GROUP BY g.sex";

$sexChart = [];
foreach ($db->query($dbQuery) AS $key => $row) {
    $sexChart[$key]['label'] = $row['sex'];
    $sexChart[$key]['value'] = $row['sexnum'];
}

$dbQuery = "SELECT case hope when 1 then '有' ELSE '無' END AS hope, COUNT(1) AS hopenum
            FROM
            (
            SELECT ct_hope AS `hope`, ct_gift AS gift
            FROM FCF_pinkwalk.registlist UNION ALL
            SELECT att_hope AS `hope`, att_gift AS gift
            FROM FCF_pinkwalk.attendlist) g
            WHERE g.gift!=0
            GROUP BY g.hope";

$hopeChart = [];
foreach ($db->query($dbQuery) AS $key => $row) {
    $hopeChart[$key]['label'] = $row['hope'];
    $hopeChart[$key]['value'] = $row['hopenum'];
}

$dbQuery = "SELECT
                SUM(CASE WHEN YEAR(NOW())-YEAR(g.birth)<=10 THEN 1 ELSE 0 END) AS '1~10歲',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(g.birth)>10 AND YEAR(NOW())-YEAR(g.birth)<=20) THEN 1 ELSE 0 END) AS '11~20歲',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(g.birth)>20 AND YEAR(NOW())-YEAR(g.birth)<=30) THEN 1 ELSE 0 END) AS '21~30歲',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(g.birth)>30 AND YEAR(NOW())-YEAR(g.birth)<=40) THEN 1 ELSE 0 END) AS '31~40歲',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(g.birth)>40 AND YEAR(NOW())-YEAR(g.birth)<=50) THEN 1 ELSE 0 END) AS '41~50歲',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(g.birth)>50 AND YEAR(NOW())-YEAR(g.birth)<=60) THEN 1 ELSE 0 END) AS '51~60歲',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(g.birth)>60 AND YEAR(NOW())-YEAR(g.birth)<=70) THEN 1 ELSE 0 END) AS '61~70歲',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(g.birth)>70 AND YEAR(NOW())-YEAR(g.birth)<=80) THEN 1 ELSE 0 END) AS '71~80歲',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(g.birth)>80 AND YEAR(NOW())-YEAR(g.birth)<=90) THEN 1 ELSE 0 END) AS '81~90歲',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(g.birth)>90) THEN 1 ELSE 0 END) AS '91歲以上'
            FROM
            (
            SELECT ct_birth AS `birth`, ct_gift AS gift
            FROM FCF_pinkwalk.registlist UNION ALL
            SELECT att_birth AS `birth`, att_gift AS gift
            FROM FCF_pinkwalk.attendlist
            ) g
            WHERE g.birth IS NOT NULL AND g.birth!='0000-00-00' AND g.gift!=0";

$ageChart = [];
$count = 0;
foreach ($db->row($dbQuery) AS $key => $value) {
    if ($value != 0) {
        $ageChart[$count]['label'] = $key;
        $ageChart[$count]['value'] = $value;
        $count++;
    }
}

// 更新無效資料
$dbQuery = "UPDATE FCF_pinkwalk.registlist r
            LEFT JOIN FCF_pinkwalk.attendlist a ON r.idno=a.reg_id
            SET r.ct_gift=0, a.att_gift=0
            WHERE r.check_pay IS NULL AND (TIMESTAMPDIFF(DAY, r.key_date, NOW())>=5 OR r.pay_method=0 OR r.pay_mount=0) AND r.ct_gift!=0;";
$db->query($dbQuery);

?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">報名統計</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> 男女比例:
                    </div>
                    <div class="panel-body">
                        <div id="morris-donut-sex"></div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> 希望護照會員:
                    </div>
                    <div class="panel-body">
                        <div id="morris-donut-hope"></div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> 年齡層比例:
                    </div>
                    <div class="panel-body">
                        <div id="morris-donut-age"></div>
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-4 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<script>
function init() {
    Morris.Donut({
        element: 'morris-donut-sex',
        data: <?=json_encode($sexChart)?>,
        resize: true,
        colors: [
            '#CC3366',
            '#0099CC',
        ],
        formatter: function (x) { return x + "人"}
    });
    Morris.Donut({
        element: 'morris-donut-hope',
        data: <?=json_encode($hopeChart)?>,
        resize: true,
        colors: [
            '#CCC',
            '#ff4558',
        ],
        formatter: function (x) { return x + "人"}
    });
    Morris.Donut({
        element: 'morris-donut-age',
        data: <?=json_encode($ageChart)?>,
        resize: true,
        formatter: function (x) { return x + "人"}
    });
}

window.onload = init;
</script>

<?php require "adminFooter.php";?>
