<?php
require_once '../inc/cfg.php';
$fileName = basename($_SERVER['REQUEST_URI'], "?{$_SERVER['QUERY_STRING']}");
?>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <a class="navbar-brand" href="./index.php">Startmin</a>
    </div>

    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>

    <ul class="nav navbar-nav navbar-left navbar-top-links">
        <li><a href="../"><i class="fa fa-home fa-fw"></i> Website</a></li>
    </ul>

    <ul class="nav navbar-right navbar-top-links">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>admin<b class="caret"></b>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <li>
                    <a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="login.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                </li>
                <li>
                    <a href="#" <?=(($fileName == 'registerList.php' || $fileName == 'registerEdit.php') ? 'aria-expanded="true"' : '')?>>
                        <i class="fa fa-list fa-fw"></i> 報名清單<span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level <?=(($fileName == 'registerList.php' || $fileName == 'registerEdit.php') ? ' mm-collapse mm-show' : '')?>">
                        <li>
                            <a href="registerList.php?fee=0"
                                <?=($fileName == 'registerList.php' && reqParam('fee', 'get') == 0 ? 'class="active"' : '')?>>
                                <i class="fa fa-exclamation fa-fw"></i> 未完成繳費
                            </a>
                        </li>
                        <li>
                            <a href="registerList.php?fee=1"
                                <?=($fileName == 'registerList.php' && reqParam('fee', 'get') == 1 ? 'class="active"' : '')?>>
                                <i class="fa fa-check fa-fw"></i> 已完成繳費
                            </a>
                        </li>
                    </ul>
                    <li>
                        <a href="./export/exportPayList.php?pay=0" target="_blank">
                            <i class="fa fa-download fa-fw"></i> 匯出未付款名單
                        </a>
                    </li>
                    <li>
                        <a href="./export/exportPayList.php?pay=1" target="_blank">
                            <i class="fa fa-download fa-fw"></i> 匯出已付款名單
                        </a>
                    </li>
                    <li>
                        <a href="./export/exportToDonate.php" target="_blank">
                            <i class="fa fa-download fa-fw"></i> 匯出轉入捐款系統資料
                        </a>
                    </li>
                    <!-- /.nav-second-level -->
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>