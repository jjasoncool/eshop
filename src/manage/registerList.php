<?php
require "adminHead.php";
require "navbar.php";

$payfee = reqParam('fee', 'get');

?>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header"><?=$payfee == 0 ? '未' : '已'?>繳費清單</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        若是傳真報名或是郵政劃撥單，需於未繳費清單內核對資料並確認
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="register-list">
                                <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>姓名</th>
                                        <th>日期</th>
                                        <th>行動電話</th>
                                        <th>金額</th>
                                        <th></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div>
<div>
<script>
    // register list
    function init() {
        const $dataTable = $('#register-list');
        const $params = new URLSearchParams(location.search);
        if ($dataTable.length) {
            $dataTable.DataTable({
                stateSave: true,
                autoWidth: false,
                responsive: true,
                processing: true,
                serverSide: true,
                ordering: false,
                ajax: {
                    "url": "api/register/list.php",
                    "data": {fee: $params.get("fee")}
                },
                columnDefs: [
                    {
                        targets: 5,
                        "render": function ( data, type, row ) {
                            return `<button class="btn btn-primary" onclick="editRow(${row[0]});">查看</button>`;
                        }
                    }
                ]
            });

            window.editRow = (id) => {
                window.location.href = `registerEdit.php?id=${id}`;
            }
        }
    }

    window.onload = init;
</script>
<?php require "adminFooter.php";?>
