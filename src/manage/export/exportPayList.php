<?php
/**
 * 將資料庫的 EXCEL 資料匯出
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $subPage 子類別
 * @param string $action 執行的動作
 */
require_once "../../inc/cfg.php";
$paystatus = boolval(reqParam('pay', 'get'));

// 資料庫連線
$db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell;

// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();

// Set document properties
$spreadsheet->getProperties()->setCreator('台灣癌症基金會')
    ->setLastModifiedBy('台灣癌症基金會')
    ->setTitle('匯出資料')
    ->setSubject('報名資料')
    ->setDescription('資料屬於基金會所有，請勿用於未授權之用途')
    ->setKeywords('canceraway')
    ->setCategory('粉紅健走');

$where = '';
$fileName = '';
if ($paystatus) {
    $where = ' AND r.check_pay IS NOT NULL';
    $fileName = '已付款清單';
} else {
    $where = ' AND r.check_pay IS NULL';
    $fileName = '未付款清單';
}

$dbQuery = "SELECT *, r.idno as r_idno, r.key_date as r_keydate
            FROM FCF_pinkwalk.registlist r
            LEFT JOIN FCF_pinkwalk.attendlist a ON r.idno=a.reg_id
            WHERE 1=1 $where AND r.ct_gift!=0 ORDER BY r.idno";
$result = $db->query($dbQuery);


$titleStyle = [
    'font' => ['bold' => true],
];

// 這邊 title 有空白是為了配合
// $spreadsheet->getActiveSheet()->getColumnDimension(Cell\Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
$coltablehead = [
    'r_idno' => '編號      ',
    'r_keydate' => '填表日      ',
    'ct_public' => '公開捐款資訊      ',
    'ct_unit' => '聯絡人團體      ',
    'ct_name' => '聯絡人姓名      ',
    'ct_birth' => '聯絡人生日      ',
    'ct_sex' => '聯絡人性別      ',
    'ct_hope' => '聯絡人是否有希望護照          ',
    'ct_phone' => '聯絡人電話      ',
    'ct_mobile' => '聯絡人手機      ',
    'ct_email' => '聯絡人email      ',
    'ct_address' => '聯絡人地址      ',
    'ct_receipt' => '開立收據      ',
    'ct_receiptname' => '收據抬頭      ',
    'ct_uniform_no' => '統一編號      ',
    'ct_gift' => '聯絡人禮物      ',
    'pay_orderid' => '訂單編號      ',
    'pay_mount' => '報名費用      ',
    'ct_donate' => '捐款金額      ',
    'pay_method' => '支付方式      ',
    'check_pay' => '確認付款時間      ',
    'pay_return_info' => '付款資訊      ',
    'att_name' => '參加人員姓名      ',
    'att_birth' => '參加人員生日      ',
    'att_sex' => '參加人員性別      ',
    'att_hope' => '參加人員是否有希望證照          ',
    'att_phone' => '參加人員電話      ',
    'att_gift' => '參加人員禮物      ',
];

// Add some data
$rownum = 1;
$column = 1;
$spreadsheet->setActiveSheetIndex(0);
foreach ($coltablehead as $colhead) {
    $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $rownum, $colhead);
    $spreadsheet->getActiveSheet()->getColumnDimension(Cell\Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
    $column++;
}
$rownum++;

$lastID = 0;
foreach ($result as $row) {
    // 每列欄位開始處
    $column = 1;
    $row = dataConvert($row);
    // 填入欄位資料
    foreach ($coltablehead as $columnName => $colhead) {
        if ($lastID == $row['r_idno'] && $column == 1) {
            $spreadsheet->getActiveSheet()->mergeCells("A{$rownum}:V{$rownum}");
        } elseif ($row['ct_receipt'] == '個人報名' && $column > intval(count($coltablehead)-6) && empty($row['reg_id'])) {
            // 個人報名沒有參加人員資料(聯絡人資料即是參加人員)
        } else {
            if ($column == intval(count($coltablehead)-6) && !empty($row['reg_id'])) {
                if ($lastID != $row['r_idno']) {
                    $rownum++;
                }
                $spreadsheet->getActiveSheet()->mergeCells("A{$rownum}:V{$rownum}");
            }
            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $rownum, $row[$columnName]);
        }
        $column++;
    }
    $rownum++;
    $lastID = $row['r_idno'];
}

// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('付款清單');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment;filename=\"$fileName.xlsx\"");
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit();

function dataConvert($row) {
    // 資料處理
    $row['ct_public'] = ($row['ct_public'] == 1 && !empty($row['ct_public'])) ? '同意' : '不同意';
    $row['pay_mount'] -= $row['ct_donate'];
    $row['ct_sex'] = ($row['ct_sex'] == 'male' && !empty($row['ct_sex'])) ? '男' : '女';
    $row['ct_hope'] = ($row['ct_hope'] == 1 && !empty($row['ct_hope'])) ? '有' : '無';
    switch ($row['ct_receipt']) {
        case '1':
            $row['ct_receipt'] = '個人報名';
            break;
        case '2':
            $row['ct_receipt'] = '團體報名分別開立收據';
            break;
        case '3':
            $row['ct_receipt'] = '團體報名統一開立收據';
            break;
        case '4':
            $row['ct_receipt'] = '不需要捐款收據';
            break;

        default:
            $row['ct_receipt'] = '錯誤資料';
            break;
    }
    switch ($row['ct_gift']) {
        case '1':
            $row['ct_gift'] = '「EXERCISE&RELAX」（黃色毛巾：規律運動、心情放鬆減低工作壓力）';
            break;
        case '2':
            $row['ct_gift'] = '「SCREENING&FIT」（粉色毛巾：定期篩檢&體重控制）';
            break;
        case '3':
            $row['ct_gift'] = '「VIGGIES&FRUITS」（綠色毛巾：蔬果彩虹579）';
            break;

        default:
            $row['ct_gift'] = '未完成付款流程';
            break;
    }
    switch ($row['pay_method']) {
        case '1':
            $row['pay_method'] = '線上刷卡';
            break;
        case '2':
            $row['pay_method'] = 'LINEPAY';
            break;
        case '3':
            $row['pay_method'] = '信用卡授權傳真';
            break;
        case '4':
            $row['pay_method'] = '郵政劃撥';
            break;
        case '4':
            $row['pay_method'] = '現金支付';
            break;

        default:
            $row['pay_method'] = '未完成付款流程';
            break;
    }
    $row['att_sex'] = ($row['att_sex'] == 'male' && !empty($rowp['att_sex'])) ? '男' : '女';
    $row['att_hope'] = ($row['att_hope'] == 1 && !empty($rowp['att_hope'])) ? '有' : '無';
    switch ($row['att_gift']) {
        case '1':
            $row['att_gift'] = '「EXERCISE&RELAX」（黃色毛巾：規律運動、心情放鬆減低工作壓力）';
            break;
        case '2':
            $row['att_gift'] = '「SCREENING&FIT」（粉色毛巾：定期篩檢&體重控制）';
            break;
        case '3':
            $row['att_gift'] = '「VIGGIES&FRUITS」（綠色毛巾：蔬果彩虹579）';
            break;

        default:
            $row['att_gift'] = '未選禮品';
            break;
    }

    return $row;
}