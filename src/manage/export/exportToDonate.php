<?php
/**
 * 將資料庫的 EXCEL 資料匯出
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $subPage 子類別
 * @param string $action 執行的動作
 */
require_once "../../inc/cfg.php";
$paystatus = boolval(reqParam('pay', 'get'));

// 資料庫連線
$db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Cell;

// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();

// Set document properties
$spreadsheet->getProperties()->setCreator('台灣癌症基金會')
    ->setLastModifiedBy('台灣癌症基金會')
    ->setTitle('匯出資料')
    ->setSubject('報名資料')
    ->setDescription('資料屬於基金會所有，請勿用於未授權之用途')
    ->setKeywords('canceraway')
    ->setCategory('粉紅健走');

$fileName = 'donate_export';

$dbQuery = "SELECT r.*, CONCAT(r.ct_phone, '/', r.ct_mobile) as r_phone, r.idno as r_idno, r.key_date as r_keydate,
            CONCAT('參加人員:', GROUP_CONCAT(DISTINCT a.att_name SEPARATOR ', ')) AS `attendList`
            FROM FCF_pinkwalk.registlist r
            LEFT JOIN FCF_pinkwalk.attendlist a ON r.idno=a.reg_id
            WHERE 1=1 AND r.check_pay IS NOT NULL GROUP BY r.idno ORDER BY r.idno";
$result = $db->query($dbQuery);

// 撈參加人員
$dbQuery = "SELECT * FROM FCF_pinkwalk.attendlist WHERE reg_id IN (SELECT idno FROM FCF_pinkwalk.registlist WHERE ct_receipt=2)";
$attresult = $db->query($dbQuery);
$attSeparate = [];
foreach ($attresult as $attRow) {
    $attSeparate[$attRow['reg_id']][] = $attRow;
}

$titleStyle = [
    'font' => ['bold' => true],
];

// 這邊 title 有空白是為了配合
// $spreadsheet->getActiveSheet()->getColumnDimension(Cell\Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
$coltablehead = [
    'r_idno' => '編號      ',
    'r_keydate' => '填表日      ',
    'check_pay' => '確認付款時間      ',
    'ct_name' => '聯絡人姓名      ',
    'ct_sex' => '聯絡人性別      ',
    'r_phone' => '聯絡人電話      ',
    'ct_email' => '聯絡人email      ',
    'pay_mount' => '報名費用      ',
    'pay_method' => '捐款方式      ',
    'pay_catelogy' => '捐款類別      ',
    'ct_receiptname' => '收據抬頭      ',
    'ct_uniform_no' => '統一編號/身分證字號      ',
    'ct_address' => '收據地址      ',
    'ct_invoiceName' => '收據姓名      ',
    'attendList' => '參加名單  ',
    'ct_public' => '公開捐款資訊(1:同意 0:不同意)  ',
];

$attColHead = [
    1 => 'reg_id',
    2 => 'key_date',
    3 => 'check_pay',
    4 => 'att_name',
    5 => 'att_sex',
    6 => 'att_phone',
    8 => 'avgMount',
    9 => 'pay_method',
    10 => 'pay_catelogy',
    11 => 'ct_receiptname',
    13 => 'ct_address',
    14 => 'att_name',
    16 => 'ct_public',
];

// Add some data
$rownum = 1;
$column = 1;
$spreadsheet->setActiveSheetIndex(0);
foreach ($coltablehead as $colhead) {
    $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $rownum, $colhead);
    $spreadsheet->getActiveSheet()->getColumnDimension(Cell\Coordinate::stringFromColumnIndex($column))->setAutoSize(true);
    $column++;
}
$rownum++;

$lastID = 0;
foreach ($result as $row) {
    // 每列欄位開始處
    $column = 1;
    if (isset($attSeparate[$row['idno']])) {
        // 處理金額
        $peopleNum = count($attSeparate[$row['idno']]) + 1;
        $avgMount = ($row['pay_mount'] - $row['ct_donate']) / $peopleNum;
        $row['pay_mount'] = $avgMount + $row['ct_donate'];
    }
    $row = dataConvert($row);
    // 填入欄位資料
    foreach ($coltablehead as $columnName => $colhead) {
        $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $rownum, $row[$columnName]);
        $column++;
    }
    // 存在參加人員
    if (isset($attSeparate[$row['idno']])) {
        foreach ($attSeparate[$row['idno']] as $attRow) {
            $attRow = array_merge($attRow, $row);
            $attRow['avgMount'] = $avgMount;
            $rownum++;
            foreach ($attColHead as $colIndex => $colName) {
                $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($colIndex, $rownum, $attRow[$colName]);
            }
        }
    }
    $rownum++;
}

// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle('付款清單');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment;filename=\"$fileName.xlsx\"");
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit();

function dataConvert($row) {
    // 資料處理
    $row['ct_sex'] = ucfirst($row['ct_sex']);
    $row['ct_invoiceName'] = $row['ct_name'];

    if (substr($row['r_phone'], 0, 1) === '/') {
        $row['r_phone'] = substr($row['r_phone'], 1);
    }
    // 收據抬頭要附加姓名
    if (strcmp($row['ct_receiptname'], $row['ct_name']) != 0) {
        $row['ct_receiptname'] = trim("{$row['ct_receiptname']} {$row['ct_name']}");
    }

    // 備註參加名單需要加上額外捐款資訊
    if (empty($row['attendList'])) {
        $row['attendList'] = "額外捐款:{$row['ct_donate']}";
    } else {
        $row['attendList'] .= "\n額外捐款:{$row['ct_donate']}";
    }
    // 是否需要開收據
    switch ($row['ct_receipt']) {
        case '1':
            $row['attendList'] = "個人報名\n{$row['attendList']}";
            break;
        case '2':
            $row['attendList'] = "團體報名分別開立收據\n{$row['attendList']}";
            break;
        case '3':
            $row['attendList'] = "團體報名統一開立收據\n{$row['attendList']}";
            break;
        case '4':
            $row['attendList'] = "不需要捐款收據\n{$row['attendList']}";
            break;

        default:
            $row['attendList'] = "錯誤資料\n{$row['attendList']}";
            break;
    }

    switch ($row['pay_method']) {
        case '1':
            $row['pay_method'] = '信用卡';
            $row['pay_catelogy'] = 'Web';
            break;
        case '2':
            $row['pay_method'] = 'LinePay';
            $row['pay_catelogy'] = 'Web';
            break;
        case '3':
            $row['pay_method'] = '信用卡';
            $row['pay_catelogy'] = 'One';
            break;
        case '4':
            $row['pay_method'] = '郵政劃撥';
            $row['pay_catelogy'] = 'One';
            break;
        case '5':
            $row['pay_method'] = '現金';
            $row['pay_catelogy'] = 'One';
            break;

        default:
            $row['pay_method'] = '錯誤資料';
            $row['pay_catelogy'] = '';
            break;
    }
    return $row;
}