<?php
$index = false;
include "./inc/header.php";
// 活動日期
$startDate = '2020/9/27';
?>
<!-- One -->
    <section id="One" class="wrapper style3">
        <div class="inner">
            <header class="align-center">
                <img src="images/contact.png" style="max-height:300px; max-width:100%">
            </header>
        </div>
    </section>

<!-- Two -->
    <section id="two" class="wrapper style2">
        <div class="inner">
            <div class="box">
                <div class="content" style="margin: 0 1rem;">
                    <header class="align-center">
                        <img src="./images/pin.png" alt="" width="32px" heigth="32px" style="vertical-align: baseline">
                        <h2>聯絡我們</h2>
                    </header>
                    <h3>台灣癌症基金會</h3>
                    <ol>
                        <li>電話 02-8787-9907</li>
                        <li>傳真 02-8787-9222</li>
                        <li>Email: <a href="mailto:5aday@canceraway.org.tw">5aday@canceraway.org.tw</a></li>
                        <li><a href="https://www.canceraway.org.tw/" target="_blank">官方網站</a></li>
                        <li><a href="https://www.facebook.com/canceraway" target="_blank">FaceBook (臉書)</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
<?php include "./inc/footer.php"; ?>
