<?php
$index = false;
include "./inc/header.php";
?>
<!-- One -->
    <section id="One" class="wrapper style3">
        <div class="inner">
            <header class="align-center">
                <img src="images/gift.png" style="max-height:300px; max-width:100%">
            </header>
        </div>
    </section>

<!-- Two -->
    <section id="two" class="wrapper style2">
        <div class="inner">
            <h1 class="icon fa-gift"> 禮物眾多，請自行攜帶提袋裝入贈品哦！</h1>
            <div class="box">
                <div class="content">
                    <?php
                        $goTakeGift1 = '只要成功報名健走，活動當日即可獲得<b><span>《某人日常》運動毛巾（三選一）與提袋</span></b> (禮物樣式以實體為準)，限量4,500組
                                        <div class="row">
                                            <div class="6u 12u$(small)">
                                                <span class="image">
                                                    <img src="/images/yellow.png" style="max-width:100%;">
                                                    「EXERCISE&RELAX」（黃色毛巾：規律運動、心情放鬆減低工作壓力）
                                                </span>
                                                <span class="image">
                                                    <img src="/images/pink.png" style="max-width:100%;">
                                                    「SCREENING&FIT」（粉色毛巾：定期篩檢&體重控制）
                                                </span>
                                                <span class="image">
                                                    <img src="/images/green.png" style="max-width:100%;">
                                                    「VIGGIES&FRUITS」（綠色毛巾：蔬果彩虹579）
                                                </span>
                                            </div>
                                            <div class="6u 12u$(small)">
                                                <img src="/images/bag.jpg" style="max-height:430px; max-width:50%; margin: 0 auto; display:block;">
                                                <span style="text-align: center; margin: 0 auto; display:block;">「某人日常」提袋</span>
                                            </div>
                                        </div>';

                        $goTakeGift2 = '<p>憑個人報到證成功報到即有超多加值好禮送給您！</p>
                                        <p>【叮寧小黑蚊防蚊凝露隨身包*1包、陽光智慧抗老換膚防曬乳*1包、施巴女性護潔體驗包*1包、施巴潔膚露體驗包*1包、屈臣氏迷你柔濕巾*1包、屈臣氏醫療防護口罩(成人)*1包、晨間廚房瓶裝水*1包、可果美果汁*1包】</p>
                                        <div class="row">
                                            <div class="4u 12u$(small)"><img src="images/gift3.jpg" style="max-width:100%;"></div>
                                            <div class="4u 12u$(small)"><img src="images/gift4.jpg" style="max-width:100%;"></div>
                                            <div class="4u 12u$(small)"><img src="images/gift5.jpg" style="max-width:100%;"></div>
                                        </div>';
                        $goTakeGift3 = '<p>完成健走路線後，憑大會手冊之折返點蓋章，除了可領取「完走證明」，還有更多完走好禮讓您拿到手軟！</p>
                                        <p>【牛頭牌金色蔬菜玉米粒*1罐、蘇菲彈力貼身Happy Catch日用超薄潔翼*1包、杏一折價券*1張、臺糖罐裝水(送完為止)*1瓶、維維樂固齒敏專業護理牙膏*1條、
                                        明治明倍適杯*2杯、CARMEX小蜜媞修護唇膏 經典原味豹紋圓罐限量版*1個 、Weet-Bix澳洲全榖片(麥香)隨身包*1包(送完為止)】</p>
                                        <div class="row">
                                            <div class="4u 12u$(small)"><img src="images/gift6.jpg" style="max-width:100%;"></div>
                                            <div class="4u 12u$(small)"><img src="images/gift7.jpg" style="max-width:100%;"></div>
                                            <div class="4u 12u$(small)"><img src="images/gift8.jpg" style="max-width:100%;"></div>
                                        </div>';
                        $goTakeGift4 = '<p>成功報到後，將大會手冊內頁抽獎券投入抽獎箱，即可參加現場抽獎，超優爆棚好禮等您來抽！</p>
                                        <p>【Apple Watch、窈窕活力健美機、Joie時尚嬰兒手推車、Diadora 球鞋、精品膠囊咖啡機、正官庄和愛樂本、正官庄高麗蔘石榴飲、施巴伴手禮盒(潔膚露200ml+潤膚乳液200ml)、
                                        One Day Bio法國煥采賦活新肌精露、QV重度修護乳膏、曖上你清潔膚隨手寶、中華海洋生技褐藻外用修復凝膠、中華海洋生技逆齡奇肌面膜、幼齒健木糖醇糖果組、…等】</p>
                                        <div class="row">
                                            <div class="4u 12u$(small)"><img src="images/gift9.jpg" style="max-width:100%;"></div>
                                            <div class="4u 12u$(small)"><img src="images/gift10.jpg" style="max-width:100%;"></div>
                                            <div class="4u 12u$(small)"><img src="images/gift11.jpg" style="max-width:100%;"></div>
                                        </div>';

                        $itemArray = [
                            "好康1-報名禮" => $goTakeGift1,
                            "陸續增加中" => '活動好禮陸續增加中...敬請期待!!'
                            // "好康2-報到禮" => $goTakeGift2,
                            // "好康3-完走禮" => $goTakeGift3,
                            // "好康4-現場抽獎" => $goTakeGift4,
                        ];

                        foreach ($itemArray as $title => $content) {
                            echo "<header>
                                    <h3>
                                        <img src=\"./images/pin.png\" alt=\"\" width=\"32px\" heigth=\"32px\" style=\"vertical-align: baseline\">
                                        {$title}
                                    </h3>
                                </header>
                                <div style=\"margin:0 3em;\">{$content}</div>";
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php include "./inc/footer.php"; ?>
