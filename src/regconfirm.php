<?php
require_once "./inc/cfg.php";
$db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);

// 聯絡人資料
$basicData = [
    'ct_public' => ['public', 'boolean'],
    'ct_unit' => ['unitName', 'string'],
    'ct_name' => ['ContactName', 'string'],
    'ct_sex' => ['sex', 'string'],
    'ct_birth' => ['birthday', 'date'],
    'ct_hope' => ['hopePass', 'boolean'],
    'ct_phone' => ['phone', 'string'],
    'ct_mobile' => ['mobile', 'string'],
    'ct_email' => ['email', 'string'],
    'ct_address' => ['address', 'string'],
    'ct_gift' => ['gift', 'int'],
    'ct_receipt' => ['receipt', 'int'],
    'ct_receiptname' => ['receiptTitle', 'string'],
    'ct_donate' => ['donate', 'int'],
    'ct_uniform_no' => ['uniNumber', 'string'],
    'pay_mount' => ['payMount', 'int'],
];

foreach ($basicData as $dbColName => $input) {
    $inputData[$dbColName] = reqParam($input[0], 'post', $input[1]);
}

// 金額不可能小於420
if ($inputData['pay_mount'] < 420) {
    header("Location: /");
    exit();
}

// 準備基本資料(主檔，付款主要資訊跟著這一條)
$dt = new dateTime();
$now = $dt->format('Y-m-d H:i:s');
$orderId = $dt->format('YmdHis') . mt_rand();
$recPeople = [
    "key_date" => $now,
    "pay_orderid" => $orderId,
];

// 自動產生SQL
$insColArray = array_merge($recPeople, $inputData);
$insColStr = implode('`,`', array_keys($insColArray));
$insParaStr = implode(",", array_fill(0, count($insColArray), "?"));
// SQL 用基本資料產出
$dbQuery = "INSERT INTO FCF_pinkwalk.registlist (
    `{$insColStr}`
) VALUES (
    {$insParaStr}
)";
$db->query($dbQuery, array_values($insColArray));
$regid = $db->lastInsertId();

// 參加名單(子表，報名人有哪些，使用 reg_id 對應主檔)
$subData = [
    'att_name',
    'att_sex',
    'att_phone',
    'att_birth',
    'att_hope',
    'att_gift',
    'reg_id',
    'key_date',
];
$insColStr = implode('`,`', $subData);
$insParaStr = '';
$insParaArr = [];
$attendPeopArray = array_filter(json_decode(rawurldecode(reqParam('attendPeop', 'post')), true, JSON_UNESCAPED_UNICODE));

if (count($attendPeopArray) > 0) {
    $insParaArr = [];
    $insQueryArr = [];
    $insIndex = 0;
    // 準備資料
    foreach ($attendPeopArray as $row) {
        $insParaArr[$insIndex]['att_name'] = $row['name'];
        $insParaArr[$insIndex]['att_sex'] = $row['sex'];
        $insParaArr[$insIndex]['att_phone'] = $row['phone'];
        $insParaArr[$insIndex]['att_birth'] = $row['birth'];
        $insParaArr[$insIndex]['att_hope'] = intval($row['hope']);
        $insParaArr[$insIndex]['att_gift'] = $row['gift'];
        $insParaArr[$insIndex]['reg_id'] = $regid;
        $insParaArr[$insIndex]['key_date'] = $now;
        // 組 query
        $insParaStr .= implode(",", array_fill(0, count($insParaArr[$insIndex]), "?")) . '),(';
        $insQueryArr = array_merge($insQueryArr, array_values($insParaArr[$insIndex]));
        $insIndex++;
    }
    $insParaStr = substr($insParaStr, 0, -3);

    $dbQuery = "INSERT INTO FCF_pinkwalk.attendlist (
                    `{$insColStr}`
                ) VALUES ({$insParaStr})";
    $db->query($dbQuery, array_values($insQueryArr));
}

// 將資訊存到 session 內
session_start();
$_SESSION["att_count"] = count($attendPeopArray) + 1;
$_SESSION["pay_mount"] = $inputData['pay_mount'];
$_SESSION["orderId"] = $orderId;
// 資料庫主檔ID
$_SESSION["regid"] = $regid;
// 設定逾時
$_SESSION['LAST_ACTIVITY'] = time();

// 選擇付款方式
header("Location: payment.php");
