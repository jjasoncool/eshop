<?php

require_once '../vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

$mail = new PHPMailer();
$mail->IsSMTP(); //設定使用SMTP方式寄信
$mail->SMTPAuth = true; //設定SMTP需要驗證
$mail->SMTPSecure = "ssl"; // Gmail的SMTP主機需要使用SSL連線
$mail->Host = SMTP_SERVER; //Gamil的SMTP主機
$mail->Port = 465; //Gamil的SMTP主機的埠號(Gmail為465)。
$mail->CharSet = "utf-8"; //郵件編碼
$mail->SetFrom(SMTP_USER, '台灣癌症基金會'); //寄件者
$mail->Username = SMTP_USER; //Gamil帳號
$mail->Password = SMTP_PASS; //Gmail密碼

if ($method == 1 || $method == 2) {
    $mail->Subject = "粉紅健走報名繳款成功通知信";
    $body = <<<EOD
            <body>
            <p>{$result['ct_name']}先生 / 小姐 您好,</p>
            <p>您已成功完成繳費及報名作業，粉紅健走活動小組將陸續以郵局掛號寄出「報到證」、<br>
            郵局平信寄出「捐款收據」。<font color='#FF0000'>活動當天請務必攜帶「報到證」為活動報名憑證哦！</font></p>
            <p>報名人員： </p><p>{$result['ct_unit']}-{$result['ct_name']}</p>
            <p>總金額：{$result['pay_mount']}元(含額外捐款)</p>
            <p>若您有任何與活動相關的問題歡迎電洽：<br>台灣癌症基金會<br>02-8787-9907分機206<br>傳真：02-8787-9222 <br><br>再次感謝您對於本活動的熱情支持!</p>
            <p>粉紅健走活動小組</p>
            </body>
            EOD;

} elseif ($method == 3 || $method == 4) {
    // TODO: 變更繳費狀態時發一封信通知成功
    $mail->Subject = "已收到您的報名，請於3個工作天內繳款";
    $body = <<<EOD
            <body>
            <p>{$result['ct_name']}先生 / 小姐 您好,</p>
            <p>您已成功傳送報名資料，再次提醒您務必於3個工作天內傳真信用卡授權表格<br>
            或至郵局完成繳款並傳真劃撥單到02-87879222後，來電02-87879907分機206<br>
            確認是否傳真成功，待粉紅健走小組寄出報名完成確認email後，才算完成報名<br>逾期系統將會自動取消報名資料。</p>
            <p>報名人員： </p><p>{$result['ct_unit']}-{$result['ct_name']}</p>
            <p>總金額：{$result['pay_mount']}元(含額外捐款)</p>
            <p><a href='https://{$_SERVER["SERVER_NAME"]}/annex/PinkWalk_Fax.doc'>下載刷卡傳真表格</a></p>
            <p><a href='https://{$_SERVER["SERVER_NAME"]}/annex/PinkWalk_Post.doc'>下載郵政劃撥表格</a></p>
            <p>若您有任何與活動相關的問題歡迎電洽：<br>台灣癌症基金會<br>02-8787-9907分機206<br>傳真：02-8787-9222 <br><br>再次感謝您對於本活動的熱情支持!</p>
            <p>粉紅健走活動小組</p>
            </body>
            EOD;
}

$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
$mail->MsgHTML($body);

// 寄信者
$mail->AddAddress($result['ct_email'], $result['ct_name']);
$mail->AddBCC('fcfserverlog@gmail.com');

if (!$mail->send()) {
    error_log('Mailer Error: ' . $mail->ErrorInfo);
} else {
    error_log(date("Y-m-d H:i:s") . " Message sent to {$result['ct_email']} (id: {$regid}) !");
}
