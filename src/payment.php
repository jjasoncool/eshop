<?php
require_once "./inc/cfg.php";
session_start();
if (empty($_SESSION) || isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 900)) {
    // 15分鐘逾時
    session_destroy(); // destroy session data in storage
    header("location: ./register.php");
    exit();
} else {
    // 上一頁回到此頁面，重設逾時時間
    $_SESSION['LAST_ACTIVITY'] = time();
    $pay_mount = $_SESSION["pay_mount"];
    $orderId = $_SESSION["orderId"];
    $regid = $_SESSION["regid"];

    $db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);
    $dbQuery = "SELECT * FROM FCF_pinkwalk.registlist WHERE idno=?";
    $result = $db->row($dbQuery, [$regid]);
}

// 如果是 post 決定付款方式
// TODO: 若是先前已有選擇付款方式，再回到此頁的時候，要確定未付款(check_pay=NULL)才可變更付款方式
if (!empty($_POST) && empty($result['check_pay'])) {
    $pay_method = intval(reqParam('pay_method', 'post'));
    $dbQuery = "UPDATE FCF_pinkwalk.registlist SET pay_method=? WHERE idno=?";
    $db->query($dbQuery, [$pay_method, $regid]);

    // 跳轉不同的付款頁面
    switch ($pay_method) {
        case '1':
            header("Location: ./payment/ecPay/donatePaycard.php");
            break;
        case '2':
            header("Location: ./payment/LinePay/Reserve.php");
            break;
        case '3':
            header("Location: pay_success.php?n=3");
            break;
        case '4':
            header("Location: pay_success.php?n=4");
            break;

        default:
            echo '<script>alert("資料錯誤，請重新填寫");</script>';
            header("Location: /");
            break;
    }
    exit();
}

// header.php 內，若設定為false，呈現單頁樣式，true 為主頁樣式
$index = false;
include "./inc/header.php";
?>

<!-- One -->
    <section id="One" class="wrapper style3">
        <div class="inner">
            <header class="align-center">
                <img src="images/register.png" style="max-height:300px; max-width:100%">
            </header>
        </div>
    </section>

<!-- Two -->
    <section id="two" class="wrapper style2">
        <div class="inner">
            <div class="box">
                <div class="content">
                    <header class="align-center">
                        <img src="./images/pin.png" alt="" width="32px" heigth="32px" style="vertical-align: baseline">
                        <h2>請選擇繳款方式</h2>
                    </header>
                    <form method="post">
                        <h3>總金額</h3>
                        <h4>共 <?=$_SESSION["att_count"]?> 人，總計 <span style="color: red;"><?=$pay_mount?></span> 元</h4>

                        <div>
                            <input type="radio" name="pay_method" id="credit" value="1" required checked>
                            <label for="credit">信用卡線上繳費(使用綠界金流線上SSL加密刷卡)</label>
                        </div>
                        <div>
                            <input type="radio" name="pay_method" id="linepay" value="2" required>
                            <label for="linepay">LINEPAY繳費</label>
                        </div>
                        <div>
                            <input type="radio" name="pay_method" id="authfax" value="3" required>
                            <label for="authfax">信用卡授權傳真(下載列印傳真授權書，傳真至02-8787-9222)</label>
                        </div>
                        <div>
                            <input type="radio" name="pay_method" id="post" value="4" required>
                            <label for="post">郵政劃撥後傳真收據</label>
                        </div>
                        <ul class="actions">
                            <li><input class="special" type="submit" value="送出"></li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php include "./inc/footer.php"; ?>
