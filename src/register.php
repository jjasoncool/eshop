<?php
$index = false;
session_start();
include "./inc/header.php";

$db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);
if (isset($_SESSION) && isset($_SESSION["regid"])) {
    $regid = $_SESSION["regid"];
    $dbQuery = "SELECT * FROM FCF_pinkwalk.registlist WHERE idno=?";
    $result = $db->row($dbQuery, [$regid]);
    $dbQuery = "SELECT att_name AS `name`,
                    att_sex AS `sex`,
                    att_phone AS `phone`,
                    att_birth AS `birth`,
                    att_hope AS `hope`,
                    att_gift AS `gift`
                FROM FCF_pinkwalk.attendlist WHERE reg_id=?";
    $attendPeop = json_encode($db->query($dbQuery, [intval($regid)]));
}

$dbQuery = "SELECT gift, 1250-COUNT(1) AS giftnum
            FROM
            (
            SELECT ct_gift AS `gift`
            FROM FCF_pinkwalk.registlist UNION ALL
            SELECT att_gift AS `gift`
            FROM FCF_pinkwalk.attendlist) g
            WHERE g.gift!=0
            GROUP BY g.gift";

// 先宣告
$giftShow = [1 => 1250, 2 => 1250, 3 => 1250];
$giftCount = $db->query($dbQuery);
foreach ($giftCount as $gift) {
    $giftShow[$gift['gift']] = $gift['giftnum'];
}

?>
<!-- One -->
    <section id="One" class="wrapper style3">
        <div class="inner">
            <header class="align-center">
                <img src="images/register.png" style="max-height:300px; max-width:100%">
            </header>
        </div>
    </section>

<!-- Two -->
    <section id="two" class="wrapper style2">
        <div class="inner">
            <h3>歡迎報名健走活動，請填寫個人或團體聯絡人資料，報名費420元/人，10人(含)以上報名享有優惠價400元/人，系統自動計算</h3>
            <h5>團體報名：請先填寫團體聯絡人的資料後，選擇捐款收據開立方式（分開開立或統一開一張），即可開始新增團體成員名單唷！</h5>
            <h4><b>☆☆重要注意事項☆☆</b></h4>
            <ul style="margin:0;">
                <li>報名時請確定毛巾好禮的選項</li>
                <li>毛巾每款數量有限，盡早報名避免向隅～</li>
                <li>為保障每個參加者的權益，送出報名後，即無法更改，當天也無法換款唷！</li>
            </ul>
            <span>決定好了嗎？下好離手囉～ ★</span>
            <div class="box">
                <div class="content">
                    <form method="post" action="regconfirm.php" id="regform">
                        <h4>報名聯絡人</h4>
                        <div class="3u$ 12u$(xsmall)">
                            <input type="checkbox" id="public" name="public" value="1" <?=(!isset($result['ct_public']) || $result['ct_public'] == 0) ? 'checked' : ''?>>
                            <label for="public"><b>本人同意公開捐款資訊</b></label>
                        </div>
                        <div class="row uniform">
                            <div class="6u 12u$(xsmall)">
                                <input type="text" name="unitName" id="unitName" value="<?=isset($result['ct_unit']) ? $result['ct_unit'] : ''?>" placeholder="單位或團體名稱">
                            </div>
                            <div class="6u$ 12u$(xsmall)">
                                <input type="text" name="ContactName" id="ContactName" value="<?=isset($result['ct_unit']) ? $result['ct_unit'] : ''?>" placeholder="姓名(必填)" required>
                            </div>
                            <!-- Break -->
                            <div class="3u 12u$(small)">
                                <input type="radio" id="male" value="male" name="sex"
                                    <?=(!isset($result['ct_sex']) || isset($result['ct_sex']) && $result['ct_sex'] == 'male') ? 'checked' : ''?>>
                                <label for="male">男</label>
                                <input type="radio" id="female" value="female" name="sex" <?=(isset($result['ct_sex']) && $result['ct_sex'] == 'female') ? 'checked' : ''?>>
                                <label for="female">女</label>
                            </div>
                            <div class="3u 12u$(xsmall)">
                                <input type="text" name="birthday" id="birthday" value="<?=isset($result['ct_birth']) ? $result['ct_birth'] : ''?>" placeholder="生日(yyyy-mm-dd)">
                            </div>
                            <div class="3u 12u$(small)">
                                <input type="text" name="phone" id="phone" value="<?=isset($result['ct_phone']) ? $result['ct_phone'] : ''?>" placeholder="連絡電話">
                            </div>
                            <div class="3u$ 12u$(small)">
                                <input type="text" name="mobile" id="mobile" value="<?=isset($result['ct_mobile']) ? $result['ct_mobile'] : ''?>" placeholder="行動電話(必填)" required>
                            </div>
                            <!-- Break -->
                            <div class="6u 12u$(xsmall)">
                                <input type="email" name="email" id="email" value="<?=isset($result['ct_email']) ? $result['ct_email'] : ''?>" placeholder="Email(必填)" required>
                            </div>
                            <div class="3u 12u$(xsmall)">
                                <input type="checkbox" id="hopePass" name="hopePass" value="1" <?=(isset($result['ct_hope']) && $result['ct_hope'] == 1) ? 'checked' : ''?>>
                                <label for="hopePass">希望護照會員</label>
                            </div>
                            <div class="3u$ 12u$(xsmall)">
                                <div class="select-wrapper">
                                    <select id="gift" name="gift">
                                        <option value="1" <?=(isset($result['ct_gift']) && $result['ct_gift'] == 1) ? 'checked' : ''?>>EXERCISE&RELAX（黃色毛巾）</option>
                                        <option value="2" <?=(isset($result['ct_gift']) && $result['ct_gift'] == 2) ? 'checked' : ''?>>SCREENING&FIT（粉色毛巾）</option>
                                        <option value="3" <?=(isset($result['ct_gift']) && $result['ct_gift'] == 3) ? 'checked' : ''?>>VIGGIES&FRUITS（綠色毛巾）</option>
                                    </select>
                                </div>
                            </div>
                            <!-- Break -->
                            <div class="12u$ 12u$(xsmall)">
                                <input type="text" name="address" id="address" value="<?=isset($result['ct_address']) ? $result['ct_address'] : ''?>" placeholder="地址(必填)" required>
                            </div>
                        </div>
                        <!-- Break -->
                        <h4 style="padding-top: 3rem;">捐款收據</h4>
                        <div class="row uniform">
                            <div class="3u 12u$(small)">
                                <input type="radio" id="personal" value="1" name="receipt"
                                    <?=(!isset($result['ct_receipt']) || isset($result['ct_receipt']) && $result['ct_receipt'] == 1) ? 'checked' : ''?>>
                                <label for="personal">個人報名者</label>
                            </div>
                            <div class="3u 12u$(small)">
                                <input type="radio" id="teamEach" value="2" name="receipt" <?=(isset($result['ct_receipt']) && $result['ct_receipt'] == 2) ? 'checked' : ''?>>
                                <label for="teamEach">團體報名:報名人員分別開立</label>
                            </div>
                            <div class="3u 12u$(small)">
                                <input type="radio" id="teamOne" value="3" name="receipt" <?=(isset($result['ct_receipt']) && $result['ct_receipt'] == 3) ? 'checked' : ''?>>
                                <label for="teamOne">團體報名:統一開立一張收據</label>
                            </div>
                            <div class="3u$ 12u$(small)">
                                <input type="radio" id="noreceipt" value="4" name="receipt" <?=(isset($result['ct_receipt']) && $result['ct_receipt'] == 4) ? 'checked' : ''?>>
                                <label for="noreceipt">不需要捐款收據</label>
                            </div>
                            <div class="6u 12u$(small)">
                                <input type="text" name="receiptTitle" id="receiptTitle" value="<?=isset($result['ct_receiptname']) ? $result['ct_receiptname'] : ''?>" placeholder="開立名稱">
                            </div>
                            <div class="6u$ 12u$(small)">
                                <input type="text" name="uniNumber" id="uniNumber" value="<?=isset($result['ct_uniform_no']) ? $result['ct_uniform_no'] : ''?>" placeholder="統一編號(身分證字號)">
                            </div>
                            <!-- Break -->
                            <div class="12u$ additionPeople" id="addRowArea">
                                <input type="hidden" name="attendPeop" id="attendPeop">
                                <input type="hidden" name="payMount" id="payMount">
                                <h4>參加人員資料 <b style="color: orange;">(不包含聯絡人)</b></h4>
                                <div class="row">
                                    <div class="2u 12u$(small)">姓名
                                        <input type="text" id="addName">
                                    </div>
                                    <div class="2u 12u$(small)">性別<br>
                                        <input type="radio" id="addmale" value="male"  name="addSex" checked>
                                        <label for="addmale">男</label>
                                        <input type="radio" id="addfemale" value="female" name="addSex">
                                        <label for="addfemale">女</label>
                                    </div>
                                    <div class="2u 12u$(small)">電話
                                        <input type="text" id="addPhone">
                                    </div>
                                    <div class="2u 12u$(small)">生日
                                        <input type="text" id="addBirth">
                                    </div>
                                    <div class="2u 12u$(small)">希望護照<br>
                                        <input type="checkbox" id="addHope" name="addHope" value="1">
                                        <label for="addHope">希望護照會員</label>
                                    </div>
                                    <div class="2u$ 12u$(small)">
                                        禮品選擇
                                        <div class="select-wrapper">
                                            <select id="addGift">
                                                <option value="1">EXERCISE&RELAX（黃色毛巾）</option>
                                                <option value="2">SCREENING&FIT（粉色毛巾）</option>
                                                <option value="3">VIGGIES&FRUITS（綠色毛巾）</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="12u 12u$(small) additionPeople">
                                <button type="button" class="special fit" id="addrow">新增人員</button>
                            </div>
                            <div class="12u 12u$(small) additionPeople">
                                <div class="table-wrapper" id="tableList">
                                    <table class="alt">
                                        <tbody>
                                            <tr>
                                                <td>姓名</td>
                                                <td>性別</td>
                                                <td>電話</td>
                                                <td>生日</td>
                                                <td>希望護照</td>
                                                <td>禮品</td>
                                                <td>刪除</td>
                                            </tr>
                                            <tr id="noPersonLabel">
                                                <td colspan="7" style="text-align: center;">目前尚未新增參加人員</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <h4 style="padding-top: 2rem;">禮品剩餘資訊 <b style="color: orange;">(請確認剩餘數量是否充足，避免向隅)</b></h4>
                        <div class="row uniform">
                            <div class="4u 12u$(small)">
                                <p>EXERCISE&RELAX（黃色毛巾）： <span style="color: red;"><?=$giftShow[1]?></span> 組</p>
                                <span class="image"><img src="/images/yellow.png" alt="黃色毛巾" width="100%"></span>
                            </div>
                            <div class="4u 12u$(small)">
                                <p>SCREENING&FIT（粉色毛巾）： <span style="color: red;"><?=$giftShow[2]?></span> 組</p>
                                <span class="image"><img src="/images/pink.png" alt="粉色毛巾" width="100%"></span>
                            </div>
                            <div class="4u 12u$(small)">
                                <p>VIGGIES&FRUITS（綠色毛巾）： <span style="color: red;"><?=$giftShow[3]?></span> 組</p>
                                <span class="image"><img src="/images/green.png" alt="綠色毛巾" width="100%"></span>
                            </div>
                        </div>
                        <h4 style="padding-top: 2rem;">報名費用統計</h4>
                        <div class="row uniform">
                            <div class="12u$">
                                <dl>
                                    <dt>共 <span id="attend_num"></span> 人報名；單人金額： <span id="mount" style="color:red;"></span> 元</dt>
                                    <dd>
                                        <p>額外捐款</p>
                                        <input type="number" name="donate" id="donate" placeholder="額外捐款" value="0" min="0" step="1"><br>
                                        <h3>總共金額為 <span style="color:red;" id="showPayMount"></span> 元</h3>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                        <!-- Break -->
                        <div class="row uniform">
                            <div class="12u$">
                                <ul class="actions">
                                    <li><input class="special" type="submit" value="送出"></li>
                                    <li><input class="alt" type="reset" id="reset" value="重新填寫"></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<script>
    'use strict';
    const genderMap = {
        'male': '男',
        'female': '女',
    };
    let attendData = {};
    let payMount = 0;
    let dataCount = 0;
    let gift1 = <?=$giftShow[1]?>;
    let gift2 = <?=$giftShow[2]?>;
    let gift3 = <?=$giftShow[3]?>;

    // TODO: 若禮物剩餘數量歸零，把 option 拿掉(但是如果在極限值1500 加加減減 option 要如何處理?)
    function checkGift() {
        if (gift1 == 0) {
            $("#gift option[value='1'], #addGift option[value='1']").remove();
        }
        if (gift2 == 0) {
            $("#gift option[value='2'], #addGift option[value='2']").remove();
        }
        if (gift3 == 0) {
            $("#gift option[value='3'], #addGift option[value='3']").remove();
        }
    }

    function groupList() {
        // 檢查是否為個人參加
        if ($("#personal").prop("checked")) {
            $(".additionPeople").hide();
            $("#receiptTitle").show();
            $("#attendPeop").val('{}');
        } else {
            if ($("#noreceipt").prop("checked")) {
                $("#receiptTitle").hide();
            } else {
                $("#receiptTitle").show();
            }
            $(".additionPeople").show();
            $("#attendPeop").val(encodeURIComponent(JSON.stringify(attendData)));
        }
    }

    function addPerson() {
        // 確認都有填寫欄位
        if ($("#addName").val() == '') {
            $("#addName").focus();
            return false;
        } else if ($("#addPhone").val() == '') {
            $("#addPhone").focus();
            return false;
        } else if ($("#addBirth").val() == '') {
            $("#addBirth").focus();
            return false;
        }

        // 新增參加名單
        attendData[++dataCount] = {
            name: $("#addName").val(),
            sex: $("[name='addSex']:checked").val(),
            phone: $("#addPhone").val(),
            birth: $("#addBirth").val(),
            hope: $("#addHope").prop("checked"),
            gift: $("#addGift").val(),
        };

        updatePayMount();
        generateTableRow(attendData);
    }

    function removePerson(id) {
        // 刪除參加名單
        delete attendData[id];
        updatePayMount();
        generateTableRow(attendData);
    }

    function generateTableRow(jsondata) {
        // 刪除未新增之資料
        $(".tableData").remove();

        if (Object.keys(jsondata).length) {
            // 用 json 產出表格
            $('#noPersonLabel').hide();
            $("#tableList tbody tr:last").after(
                Object
                .entries(jsondata)
                .map(([key, value]) => `
                    <tr class="tableData">
                        <td>${value['name']}</td>
                        <td>${genderMap[value.sex]}</td>
                        <td>${value['phone']}</td>
                        <td>${value['birth']}</td>
                        <td>${value.hope ? '有' : '無'}</td>
                        <td>${$('#addGift').find(`[value=${value.gift}]`).text()}</td>
                        <td>
                            <button
                                type="button"
                                class="button special delrow"
                                value="${key}"
                                style="height: 1.5rem; line-height: 1.5rem;"
                            >
                                刪除
                            </button>
                        </td>
                    </tr>
                `)
            );
        } else {
            $('#noPersonLabel').show();
        }

        // 綁定事件
        $(".delrow").off("click");
        $(".delrow").click(function () {
            removePerson($(this).val());
        });

        // 重設填寫欄位
        $("#addRowArea :input").val('');
        $("#addGift").val('1');
        $("#addmale").val('male');
        $("#addfemale").val('female');
        $("#addHope").prop('checked', false);

        // 參加名單
        $("#attendPeop").val(JSON.stringify(jsondata));
        $("#payMount").val(payMount);
    }

    function force2decimal(num) {
        return parseInt((num || 0) * 100) / 100;
    }

    function updatePayMount() {
        const $donate = $("#donate");
        const $attendNum = $("#attend_num");
        const $mount = $("#mount");
        const $showPayMount = $("#showPayMount");
        const $payMount = $("#payMount");

        const donate = force2decimal(parseFloat($donate.val()));
        const attendNum = Object.keys(attendData).length + 1;
        const mount = attendNum >= 10 ? 400 : 420;
        payMount = force2decimal(attendNum * mount + donate);

        $attendNum.text(attendNum);
        $mount.text(mount);
        $showPayMount.text(payMount);
        $payMount.val(payMount);
        $donate.val(donate);
    }

    function clearAttendedPeople() {
        dataCount = 0;
        attendData = {};
        generateTableRow(attendData);
    }

    function init() {
        <?php
        if (!empty($attendPeop)) {
            echo "attendData = {$attendPeop}; dataCount = Object.keys(attendData).length; generateTableRow({$attendPeop});";
        }
        ?>
        checkGift();
        groupList();
        updatePayMount();
        $("#birthday, #addBirth").flatpickr();
        $("[name='receipt']").click(groupList);

        // 新增人員
        $("#addrow").click(addPerson);

        // 重新填寫
        $("#reset").click(() => {
            $(".additionPeople").hide();
            clearAttendedPeople();
            updatePayMount();
        });

        $("#donate").on('input', updatePayMount);
    }

    window.onload = init;
</script>

<?php include "./inc/footer.php";?>
