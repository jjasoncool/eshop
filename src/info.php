<?php
$index = false;
include "./inc/header.php";
?>
<!-- One -->
    <section id="One" class="wrapper style3">
        <div class="inner">
            <header class="align-center">
                <img src="images/info.png" style="max-height:300px; max-width:100%">
            </header>
        </div>
    </section>

<!-- Two -->
    <section id="two" class="wrapper style2">
        <div class="inner">
            <h1 class="icon fa-map-marker"> 圓山花博花海廣場 2020年9月27日</h1>
            <div class="box">
                <div class="content">
                    <?php
                        $schedule = '<table>
                                    <thead>
                                        <tr>
                                            <th>時間</th>
                                            <th>活動項目</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>08：30～09：00</td>
                                            <td>參加者報到</td>
                                        </tr>
                                        <tr>
                                            <td>09：00～11：00</td>
                                            <td>健走時間</td>
                                        </tr>
                                        <tr>
                                            <td>10：30～12：00</td>
                                            <td>舞台表演與園遊會</td>
                                        </tr>
                                        <tr>
                                            <td>11：00～12：00</td>
                                            <td>好禮抽獎、有獎徵答</td>
                                        </tr>
                                    </tbody>
                                </table>';

                        $addr = '<p>圓山花博花海廣場（捷運圓山站出口往花海廣場方向）</p>';
                        $route = '<p>花博花海廣場（起/終點）由花海廣場出發，至大直橋（折返點）回花海廣場【全程約7公里】</p>
                                    <img src="./images/mapbig.gif" alt="" style="max-width: 100%;">';
                        $time = '<p>2019/07/15 起至報名額滿止</p>';
                        $fee = '<p>每人報名捐款420元 (提供捐款收據)<br><span style="color:red;">10人(含)以上報名享有優惠價400元/人</span></p>';
                        $flow = '<div class="row">
                                    <div class="4u 12u$(small)">
                                        <h4>報名參加</h4>
                                        完成報名及繳費手續
                                    </div>
                                    <div class="4u 12u$(small)">
                                        <h4>領取報到證</h4>
                                        粉紅健走小組確認報名資料後，將陸續以掛號方式寄送「報到證」、並以平信方式寄送「捐款收據」
                                    </div>
                                    <div class="4u 12u$(small)">
                                        <h4>前往活動</h4>
                                        活動當天請攜帶個人「報到證」前往現場，憑個人「報到證」領取報名禮、其他好禮及大會手冊(內含抽獎聯)
                                    </div>
                                </div>';
                        $register = '<a class="button special icon fa-wifi">網路報名:</a><br>
                                    請至<a href="register.php">【我要報名】</a>填寫資料，並選擇繳款方式，完成報名後將收到「粉紅健走報名確認」email，即完成報名手續。<br><br>
                                    <a class="button special icon fa-fighter-jet"></i>親赴報名:</a><br>
                                    請於週一~週五 上午9:30~下午5:30親洽台灣癌症基金會櫃台報名繳費（台北市松山區南京東路五段16號5樓之2）。<br><br>
                                    <a class="button special icon fa-print">傳真/郵寄報名:</a><br>
                                    <a href="/annex/PinkWalk_Form.pdf" target="_blank">【下載活動報名表】</a>請工整填寫報名資訊及付款方式(郵政劃撥or信用卡授權)，傳真到02-8787-9222，並來電02-8787-9907#205確認，即完成報名手續。';

                        $traffic = '<a class="button special icon fa-subway">搭乘捷運:</a><br>
                                    搭乘台北捷運往淡水或新北投的路線，於<u><b>圓山站</b></u>下車，抵達<u>一號出口</u>後往左過馬路即是<u>花博圓山廣場</u>，請到活動報到區報到。<br><br>
                                    <a class="button special icon fa-bus"></i>搭乘公車:</a><br>
                                    搭乘26、41、111、218直、266、266區、280、288、288區、303、303區、304承德、616、618線公車，於<u><b>明倫高中</b></u>站下車。<br>
                                    搭乘26、41、266、266區、280、288、288區、290、303、303區、304、承德、松山幹線、616、618、811線公車，於<u><b>庫倫街口站</b></u>下車。<br>
                                    搭乘21、21直達、208、208基河國宅、247區、287區、542、677、紅2、紅33、紅34線公車，於<u><b>捷運圓山站</b></u>下車。<br>
                                    <iframe class="12u 12u$(small)" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1806.9592182258782!2d121.51938615723348!3d25.070753475932012!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3442a94ef1ee06b1%3A0xf149e1aa1c551171!2z6Iqx5Y2a5YWs5ZyS5ZyT5bGx5ZyS5Y2A!5e0!3m2!1szh-TW!2stw!4v1554518234863!5m2!1szh-TW!2stw" frameborder="0" style="border:0; height:400px" allowfullscreen></iframe>
                                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBlUJtsIi_FRurx0i2WUGoxf_KaNoHmc4o"></script>';

                        $itemArray = [
                            "活動時程" => $schedule,
                            "集合地點" => $addr,
                            "健走路線" => $route,
                            "報名時間" => $time,
                            "報名費用" => $fee,
                            "參加流程" => $flow,
                            "報名繳費" => $register,
                            "交通資訊" => $traffic,
                        ];

                        foreach ($itemArray as $title => $content) {
                            echo "<header>
                                    <h3>
                                        <img src=\"./images/pin.png\" alt=\"\" width=\"32px\" heigth=\"32px\" style=\"vertical-align: baseline\">
                                        {$title}
                                    </h3>
                                </header>
                                <div style=\"margin:0 3em;\">{$content}</div>";
                        }
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php include "./inc/footer.php"; ?>
