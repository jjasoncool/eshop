<?php
// 列印授權單
require_once "../inc/cfg.php";
$name = reqParam('name', 'get');
$tel = reqParam('tel', 'get');
$mount = reqParam('mount', 'get');
?>

<!DOCTYPE html>
<html lang="zh-tw">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>傳真刷卡授權單</title>
</head>

<body onload="window.print();">
    <h2 align="center">台灣癌症基金會粉紅健走活動傳真刷卡單</h2>
    <hr>
    <ul>
        <li class="txtblackA">本次健走參與之總金額：<span><?=$mount?></span>元 </li>
        <li class="txtblackA">請於三個工作天內填妥以下表格，傳真至02-8787-9222，並來電02-87879907分機206確認，逾期將取消報名資格。</li>
    </ul>
    <table width="90%" border="2" align="center" cellpadding="4" cellspacing="0">
        <tr>
            <td colspan="4" align="center">
                <p>信用卡授權傳真</p>
            </td>
        </tr>
        <tr>
            <td width="18%" align="center" nowrap="nowrap">聯絡人姓名</td>
            <td width="33%"><?=$name?></td>
            <td width="10%" align="center" nowrap="nowrap">聯絡電話</td>
            <td width="39%"><?=$tel?></td>
        </tr>
        <tr>
            <td align="center" nowrap="nowrap">信用卡<br>
                持卡人姓名</td>
            <td>&nbsp;</td>
            <td align="center" nowrap="nowrap">卡別</td>
            <td nowrap="nowrap">
                <p>□ VISA&nbsp; □ MasterCard&nbsp;&nbsp; □ JCB</p>
            </td>
        </tr>
        <tr>
            <td align="center" nowrap="nowrap">信用卡號碼</td>
            <td colspan="3">
                <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30" align="center">-</td>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30" align="center">-</td>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30" align="center">-</td>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30">&nbsp;</td>
                        <td width="30" height="30">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" nowrap="nowrap">發卡銀行</td>
            <td>&nbsp;</td>
            <td align="center" nowrap="nowrap">信用卡有效期限</td>
            <td>________月____________年</td>
        </tr>
        <tr>
            <td align="center" nowrap="nowrap">持卡人/ 帳戶<br>
                持有人簽名</td>
            <td>
                <p>&nbsp;</p>
            </td>
            <td align="center" nowrap="nowrap">刷卡金額</td>
            <td><?=$mount?></td>
        </tr>
        <tr>
            <td align="center" nowrap="nowrap">日期</td>
            <td>&nbsp;</td>
            <td align="center" nowrap="nowrap">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <ul>
        <li>請填妥個人資料及捐款表格，傳真至02-8787-9222，捐款收據將以郵局平信寄出；如有其他相關問題，請電洽02-87879907分機206。</li>
    </ul>
</body>

</html>