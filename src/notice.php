<?php
$index = false;
include "./inc/header.php";
// 活動日期
$startDate = '2020/9/27';
?>
<!-- One -->
    <section id="One" class="wrapper style3">
        <div class="inner">
            <header class="align-center">
                <img src="images/notice.png" style="max-height:300px; max-width:100%">
            </header>
        </div>
    </section>

<!-- Two -->
    <section id="two" class="wrapper style2">
        <div class="inner">
            <div class="box">
                <div class="content">
                    <header class="align-center">
                        <img src="./images/pin.png" alt="" width="32px" heigth="32px" style="vertical-align: baseline">
                        <h2>請注意以下活動與報到細節</h2>
                    </header>
                    <ol>
                        <li>參加人員需遵守活動秩序並依指示路線前進。 </li>
                        <li>請自備提袋(贈品眾多)、輕便雨衣、雨傘、防曬用品及個人藥品。 </li>
                        <li>為響應節能減碳，請民眾多加利用大眾運輸工具。 </li>
                        <li>參加人員將由主辦單位投保大會意外責任險。 </li>
                        <li>活動當天如於健走過程中感到不適，應立即至安全處休息，切勿超出身體負荷。 </li>
                        <li>活動報到證請妥善保存，破損遺失恕不補發。 </li>
                        <li>繳費後恕不退費。 </li>
                        <li>活動當日(<?=$startDate?>)報到時，請務必出示『報到證』，方可領取活動相關贈品。 </li>
                        <li>報名紀念品、報名加值好禮、大會手冊(含摸彩券)等將於活動當日報到時發送(<?=$startDate?> AM9:30前)，逾期恕不另行通知與補發。 </li>
                        <li>報名紀念品及摸彩贈品以實物為準，不得要求更換獎品、尺寸、顏色、折換現金等。活動網頁、DM及活動海報之圖片僅供參考。 </li>
                        <li>依財政部規定中獎金額超過NT＄20,001元，依法須繳10%機會中獎稅及身分證影本。外籍中獎人士依法須繳20%機會中獎稅及居留證影本。 </li>
                        <li>摸彩中獎者，限當日現場領獎，如不在場則視同放棄得獎，恕不另行通知與補發。 </li>
                        <li>主辦單位如遇人力不可抗拒之因素，保有隨時修改、暫停或終止本活動之權利，本活動一切更動將以活動網站公告為主，主辦單位將不另行通知活動參加者，請參加者注意本活動網站公告之事項。 </li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
<?php include "./inc/footer.php"; ?>
