<?php
require_once "./inc/cfg.php";
session_start();
if (empty($_SESSION) || isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 900)) {
    // 15分鐘逾時
    session_destroy(); // destroy session data in storage
    header("location: /");
    exit();
} else {
    // 上一頁回到此頁面，重設逾時時間
    $_SESSION['LAST_ACTIVITY'] = time();
    $pay_mount = $_SESSION["pay_mount"];
    $orderId = $_SESSION["orderId"];
    $regid = $_SESSION["regid"];

    $db = new MysqlDB(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);
    $dbQuery = "SELECT * FROM FCF_pinkwalk.registlist WHERE idno=?";
    $result = $db->row($dbQuery, [$regid]);
}

$method = strval(intval(reqParam('n', 'get')));

$successFlag = true;
if ($method == 1) {
    if (reqParam('RtnCode', 'post') != 1) {
        $successFlag = false;
    }
}

$content = '';
switch ($method) {
    case '1':
        // 信用卡
    case '2':
        // LINEPAY
        // 線上付款是否成功
        if ($successFlag) {
            $content = <<<EOD
                <p><b>線上支付完成，報名資料已送出，感謝您！</b></p>
                <p>我們已寄發一封報名完成確認信到您的電子郵件信箱，<br>
                近期將以郵局掛號寄出「報到證」、郵局平信寄出「捐款收據」<br>
                活動當天請務必攜帶「報到證」為活動報名憑證哦！</p>
                <p>如有其他問題，歡迎致電02-87879907分機206</p>
            EOD;
        } else {
            $content = <<<EOD
            <p><b>線上繳費失敗！</b></p>
            <p>您的線上作業失敗，建議您聯絡您的信用卡發卡銀行、確認LINEPAY綁定之信用卡額度或信用卡驗證資訊。<br>
                您也可以更換其他種繳費方式，或其他問題歡迎致電02-87879907分機206洽詢。<br>
                再次感謝您對本活動的熱情支持！</p>
            <p>如有其他問題，歡迎致電02-87879907分機206</p>
            EOD;
        }
        break;
    case '3':
        // 信用卡授權傳真
        $name = "{$result['ct_unit']} - {$result['ct_name']}";
        $content = <<<EOD
            <p><b>您選擇使用信用卡授權傳真</b></p>
            <p>我們已寄發一封提醒信到您的Email信箱，請務必於三個工作天內
            填妥信用卡授權表格並完成傳真<span style="color: red">02-87879222</span>。<br>
            並來電02-8787-9907 分機206 確認，逾期系統將自動刪除報名資料哦！</p>
            <p>再次感謝您對本活動的熱情支持</p>
            <ul class="actions fit">
                <li><a class="button special fit" href="./annex/PinkWalk_Fax_P.php?name={$name}&tel={$result['ct_mobile']}&mount={$result['pay_mount']}">列印刷卡傳真單</a></li>
                <li><a class="button special fit" href="./annex/PinkWalk_Fax.doc">下載刷卡傳真單</a></li>
            </ul>
        EOD;
        break;
    case '4':
        // 郵政劃撥後傳真收據
        $content = <<<EOD
            <p><b>您選擇使用郵政劃撥後傳真收據</b></p>
            <p>帳號：<span style="color: red">19096916</span>　戶名：<span style="color: red">財團法人台灣癌症基金會</span></p>
            <p>我們已寄發一封提醒信至您的Email信箱，請務必於三個工作天內至郵局完成劃撥繳費，並請於劃撥單上註明聯絡人姓名電話。<br>
                繳費完成後請於劃撥收據空白處填上聯絡人姓名電話，傳真回覆至<span style="color: red">02-8787-9222</span><br>
                並請來電02-8787-9907 分機206，確認傳真收件。<br>
                本會確認繳費完成後，將Email給您報名完成確認信，即完成報名手續。</p>
            <p>請及早完成劃撥繳費並回傳劃撥收據，以利後續作業，並避免影響您的權益。<br>
                若於報名後3個工作天內未完成繳費及回傳收據，系統將自動取消報名資格。​</p>
            <ul class="actions fit">
                <li><a class="button special fit" href="./annex/PinkWalk_Post.doc">下載郵政劃撥單</a></li>
            </ul>
        EOD;
        break;
    default:
        header("Location: /");
        exit();
        break;
}

// 寄信程式
if ($successFlag) {
    require_once "./mail/sendMail.php";
} else {
    // 付款失敗，禮物不算
    $dbQuery = "UPDATE FCF_pinkwalk.registlist SET ct_gift=0 WHERE idno=?";
    $db->query($dbQuery, [$regid]);
    $dbQuery = "UPDATE FCF_pinkwalk.attendlist SET att_gift=0 WHERE reg_id=?";
    $db->query($dbQuery, [$regid]);
}

$index = false;
include "./inc/header.php";
?>
<!-- One -->
    <section id="One" class="wrapper style3">
        <div class="inner">
            <header class="align-center">
                <img src="images/register.png" style="max-height:300px; max-width:100%">
            </header>
        </div>
    </section>
    <section id="two" class="wrapper style2">
        <div class="inner">
            <div class="box">
                <div class="content">
                    <header class="align-center">
                        <img src="./images/pin.png" alt="" width="32px" heigth="32px" style="vertical-align: baseline">
                        <h2>報名資料已送出</h2>
                        <p style="color:#003D79;">完成報名之認定，以報名填寫+完成繳費手續後，報名者收到「粉紅健走報名完成確認信」才視為報名完成，非線上立即繳費者，請務必於3日內完成報名繳費並回傳繳費收據，便利後續審核報名完成作業，以避免影響您的權益。</p>
                    </header>
                    <div class="align-center">
                        <?=$content?>
                        <ul class="actions fit">
                            <li><a class="button special fit" href="/">回首頁</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include "./inc/footer.php"; ?>
