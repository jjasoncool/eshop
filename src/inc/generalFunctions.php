<?php
/**
 * 取得request參數
 *
 * @param string $index 參數名稱
 * @param string $method 傳值方法
 * @param string $type 資料型別
 * @return $param
 */
function reqParam($index, $method, $type = null)
{
    switch (strtolower($method)) {
        case 'post':
            $param = isset($_POST[$index]) ? general_htmlentities($_POST[$index], ENT_NOQUOTES) : '';
            break;

        case 'get':
        default:
            $param = isset($_GET[$index]) ? general_htmlentities($_GET[$index], ENT_NOQUOTES) : '';
            break;
    }

    if (empty($param)) {
        switch ($type) {
            case 'string':
                return strval($param);
                break;

            case 'boolean':
                return intval(boolval($param));
                break;

            case 'int':
                return intval($param);
                break;

            case 'float':
                return floatval($param);
                break;

            case 'date':
                return null;
                break;

            default:
                return $param;
                break;
        }
    } elseif (is_array($param)) {
        switch ($type) {
            case 'string':
                return array_map('strval', $param);
                break;

            case 'boolean':
                return array_map('intval', array_map('boolval', $param));
                break;

            case 'int':
                return array_map('intval', $param);
                break;

            case 'float':
                return array_map('floatval', $param);
                break;

            case 'date':
                foreach ($param as $key => $value) {
                    if (empty($value)) {
                        $param[$key] = null;
                    }
                }
            default:
                return $param;
                break;
        }
    } else {
        // 一般單一參數回傳
        switch ($type) {
            case 'string':
                return strval($param);
                break;

            case 'boolean':
                return boolval($param);
                break;

            case 'int':
                return intval($param);
                break;

            case 'float':
                return floatval($param);
                break;

            default:
                return $param;
                break;
        }
    }
}

/**
 * 可以針對一般值或是陣列去除特殊字元
 *
 * @param mixed $param
 * @return $param
 */
function general_htmlentities($param, $flag)
{
    if (is_array($param)) {
        foreach ($param as $key => $value) {
            $param[$key] = trim(htmlentities($value, $flag));
        }
    } else {
        $param = trim(htmlentities($param, $flag));
    }

    return $param;
}

/**
 * 取得相對於根目錄的路徑
 *
 * @param string $file 根目錄路徑(開頭不要有/)
 * @return $path
 */
function get_relative_path($file)
{
    // 計算與跟目錄的資料夾數量，把路徑加上../
    $folder_depth = substr_count($_SERVER["PHP_SELF"], "/");
    if ($folder_depth == false) {
        $folder_depth = 1;
    }
    $prefix = (($folder_depth - root_depth > 0) ? str_repeat("../", $folder_depth - root_depth) : "");
    // 把../加上從根目錄開始的路徑，就是完整相對路徑
    $path = $prefix . $file;
    return $path;
}

/**
 * 取得使用者ip
 *
 * @return $ip
 */
function getIP()
{
    //先判斷user是否使用代理伺服器上網
    if (isset($_SERVER["HTTP_CLIENT_IP"])) {
        $ip = $_SERVER["HTTP_CLIENT_IP"];
    } elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
        $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

/**
 * 輸入日期是否合法
 *
 * @param string $date 輸入的日期字串
 * @param string $format 符合格式
 * @return boolean
 */
function validateDate($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}
