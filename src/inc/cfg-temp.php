<?php
if (!function_exists('checkDefine')) {
    function checkDefine($defName, $defValue)
    {
        if (!defined($defName)) {
            define($defName, $defValue);
        }
    }
}

checkDefine('DB_HOST', '127.0.0.1');
checkDefine('DB_PORT', 3306);
checkDefine('DB_USER', '');
checkDefine('DB_PASS', '');
checkDefine('DB_NAME', '');
checkDefine('root_depth', '1');

checkDefine('SMTP_SERVER', '');
checkDefine('SMTP_PORT', 465);
checkDefine('SMTP_USER', '');
checkDefine('SMTP_PASS', '');

checkDefine('LINE_API_ENDPOINT', '');
checkDefine('LINE_CHANNEL_ID', '');
checkDefine('LINE_CHANNEL_SECRET', '');

checkDefine('ECPAY_API_URL', '');
checkDefine('ECPAY_ID', '');
checkDefine('ECPAY_KEY', '');
checkDefine('ECPAY_IV', '');

require_once __DIR__ . DIRECTORY_SEPARATOR . '../../vendor/autoload.php';
require_once 'conn/PDO.class.php';
require_once 'generalFunctions.php';
