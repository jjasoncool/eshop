<?php require 'cfg.php'?>
<!DOCTYPE HTML>

<html>
	<head>
		<title>台灣癌症基金會-粉紅健走</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/flatpickr.css">
        <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
	</head>
	<body>

		<!-- Header -->
			<header id="header" <?=$index ? 'class="alt"' : ''?>>
				<div class="logo">
                    <a href="index.php"><img src="images/FCF_logo.png" style="max-height:50px; max-width:100%"></a>
                </div>
				<a href="#menu" style="font-size:24px;">Menu</a>
			</header>

		<!-- Nav -->
			<nav id="menu">
				<ul class="links">
					<li><a href="index.php">首頁</a></li>
					<li><a href="info.php">活動資訊</a></li>
					<li><a href="gift.php">活動好禮</a></li>
					<li><a href="notice.php">注意事項</a></li>
					<li><a href="contact.php">聯繫資訊</a></li>
					<li><a href="register.php">我要報名</a></li>
				</ul>
			</nav>