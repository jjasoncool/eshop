        <!-- Footer -->
        <footer id="footer">
            <div class="container">
                <div class="text">
                    <span style="font-size:1.5em;">財團法人台灣癌症基金會</span>
                    <p>行政院衛生署醫字第86033641號<br>
                    洽詢專線：02-8787-9907 分機205、220<br>
                    洽詢Email：5aday@canceraway.org.tw</p>
                </div>
                <ul class="icons">
                    <li><a href="https://www.canceraway.org.tw" class="icon fa-home"><span class="label">Offical</span></a></li>
                    <li><a href="https://www.facebook.com/canceraway/" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                    <!-- <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li> -->
                    <li><a href="mailto:5aday@canceraway.org.tw" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
                </ul>
            </div>
            <div class="copyright">
                &copy;2020 台灣癌症基金會. All rights reserved.
            </div>
        </footer>

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/flatpickr.js"></script>
    </body>
</html>