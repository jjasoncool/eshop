CREATE SCHEMA `FCF_pinkwalk` DEFAULT CHARACTER SET UTF8MB4;

-- ===================================== --
--           登入後台資料庫
-- ===================================== --
-- 資料庫：FCF_pinkwalk
-- 關聯物件:users

CREATE TABLE `FCF_pinkwalk`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC));

INSERT INTO `FCF_pinkwalk`.`users` (`username`, `password`) VALUES ('admin', 'dummy');

-- 報名資料
DROP TABLE IF EXISTS FCF_pinkwalk.`registlist`;
CREATE TABLE FCF_pinkwalk.`registlist` (
    `idno` INT NOT NULL AUTO_INCREMENT,
    `key_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT '資料輸入日期',
    `ct_public` BOOLEAN NOT NULL DEFAULT 0 COMMENT '報名聯絡人是否願意公開捐款資訊 0:同意 1:不同意',
    `ct_unit` VARCHAR(30) NOT NULL DEFAULT '' COMMENT '報名聯絡人單位名稱',
    `ct_name` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '報名聯絡人姓名',
    `ct_birth` DATE NULL DEFAULT NULL COMMENT '報名聯絡人生日',
    `ct_sex` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '報名聯絡人性別 男:male 女:female',
    `ct_hope` BOOLEAN NOT NULL DEFAULT 0 COMMENT '報名聯絡人是否有希望護照 0:沒有 1:有',
    `ct_phone` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '報名聯絡人聯絡電話',
    `ct_mobile` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '報名聯絡人行動電話',
    `ct_email` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '報名聯絡人電子郵件',
    `ct_address` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '報名聯絡人通訊地址',
    `ct_receipt` INT NOT NULL DEFAULT 1 COMMENT '報名捐款收據方式 1:個人報名 2:團體報名分別開立 3:團體報名統一開立 4:不需要捐款收據',
    `ct_receiptname` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '報名捐款收據開立名稱',
    `ct_uniform_no` VARCHAR(200) NOT NULL DEFAULT '' COMMENT '統一編號',
    `ct_gift` INT NOT NULL DEFAULT 1 COMMENT '報名禮品 1:夕陽餘暉 2:某人運動 3:愛篩快檢，另外若此值為0，代表付款失敗',
    `ct_donate` INT NOT NULL DEFAULT 0 COMMENT '額外捐款',
	`pay_orderid` VARCHAR(50) NULL DEFAULT NULL COMMENT '報名API產生的訂單編號',
    `pay_mount` INT NOT NULL DEFAULT 0 COMMENT '報名費用(含額外捐款)',
    `pay_method` INT NULL DEFAULT 0 COMMENT '支付方式 1:線上刷卡 2:LINEPAY 3:信用卡授權傳真 4:郵政劃撥',
    `pay_return_info` TEXT NOT NULL DEFAULT '' COMMENT '線上支付回傳資訊/已付款備註',
    `check_pay` DATETIME NULL DEFAULT NULL COMMENT '若成功付款，塞入時間日期，未付款為NULL',
    PRIMARY KEY (`idno`) USING BTREE,
    UNIQUE INDEX `pay_orderid` (`pay_orderid`) USING BTREE
);

DROP TABLE IF EXISTS FCF_pinkwalk.`attendlist`;
CREATE TABLE FCF_pinkwalk.`attendlist` (
    `idno` INT NOT NULL AUTO_INCREMENT,
    `key_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP COMMENT '資料輸入日期',
    `reg_id` INT NULL DEFAULT NULL COMMENT '報名資料ID (registlist)',
    `att_name` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '報名聯絡人姓名',
    `att_birth` DATE NULL DEFAULT NULL COMMENT '報名聯絡人生日',
    `att_sex` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '報名聯絡人性別 男:male 女:female',
    `att_hope` BOOLEAN NOT NULL DEFAULT 0 COMMENT '報名聯絡人是否有希望護照 0:沒有 1:有',
    `att_phone` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '報名聯絡人聯絡電話',
    `att_gift` INT NOT NULL DEFAULT 1 COMMENT '報名禮品 1:夕陽餘暉 2:某人運動 3:愛篩快檢',
    PRIMARY KEY (`idno`) USING BTREE,
    INDEX `regIndex` (`reg_id`) USING BTREE
);